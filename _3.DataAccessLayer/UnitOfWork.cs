﻿using _3.DataAccessLayer.Repositories.IRepositories;
using _2.Domain2Db;
using Microsoft.EntityFrameworkCore.Storage;
using _3.DataAccessLayer.Commons;

namespace _3.DataAccessLayer;

public class UnitOfWork : IUnitOfWork
{
    private IDbContextTransaction? _transaction;
    private bool _disposed;
    //
    private readonly ApplicationDbContext _context;
    private readonly IBookRepository _bookRepository;
    private readonly ICartRepository _cartRepository;
    private readonly ICartItemRepository _cartItemRepository;
    private readonly IOrderRepository _orderRepository;
    private readonly IOrderItemRepository _orderItemRepository;
    private readonly IUserCredentialRepository _userCredentialRepository;
    private readonly IUserRepository _userRepository;
    //
    public IBookRepository BookRepository => _bookRepository;
    public ICartRepository CartRepository => _cartRepository;
    public ICartItemRepository CartItemRepository => _cartItemRepository;
    public IOrderRepository OrderRepository => _orderRepository;
    public IOrderItemRepository OrderItemRepository => _orderItemRepository;
    public IUserCredentialRepository UserCredentialRepository => _userCredentialRepository;
    public IUserRepository UserRepository => _userRepository;

    public UnitOfWork(
        ApplicationDbContext dbContext,
        IBookRepository bookRepository,
        ICartRepository cartRepository,
        ICartItemRepository cartItemRepository,
        IOrderRepository orderRepository,
        IOrderItemRepository orderItemRepository,
        IUserCredentialRepository userCredentialRepository,
        IUserRepository userRepository)
    {
        _context = dbContext;
        //
        _bookRepository = bookRepository;
        _cartRepository = cartRepository;
        _cartItemRepository = cartItemRepository;
        _orderRepository = orderRepository;
        _orderItemRepository = orderItemRepository;
        _userCredentialRepository = userCredentialRepository;
        _userRepository = userRepository;
    }

    public int SaveChanges() => _context.SaveChanges();

    public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();

    public void BeginTransaction()
    {
        _transaction = _context.Database.BeginTransaction();
    }

    public void Commit()
    {
        try
        {
            _context.SaveChanges();
            _transaction?.Commit();
        }
        finally
        {
            _transaction?.Dispose();
            _transaction = null;
        }
    }

    public async Task CommitAsync()
    {
        try
        {
            await _context.SaveChangesAsync();
            _transaction?.Commit();
        }
        finally
        {
            _transaction?.Dispose();
            _transaction = null;
        }
    }

    public void Rollback()
    {
        if (_transaction == null)
            throw new RepositoryException("No transaction to rollback");
        _transaction.Rollback();
        _transaction.Dispose();
        _transaction = null;
    }

    public async Task RollbackAsync()
    {
        if (_transaction == null)
            throw new RepositoryException("No transaction to rollback");
        await _transaction.RollbackAsync();
        _transaction.Dispose();
        _transaction = null;
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _transaction?.Dispose();
                _context.Dispose();
            }

            _disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
