# Infrastructure

```bash
dotnet new classlib

dotnet add reference "../_2.Domain2Db/_2.Domain2Db.csproj"
dotnet add reference "../_3.Share/_3.Share.csproj"
#
dotnet build
#
dotnet add package AutoMapper
dotnet add package BCrypt.Net-Next
dotnet add package Microsoft.AspNetCore.Http
#
```
