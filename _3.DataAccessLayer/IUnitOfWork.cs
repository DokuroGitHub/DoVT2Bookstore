﻿using _3.DataAccessLayer.Repositories.IRepositories;

namespace _3.DataAccessLayer;

public interface IUnitOfWork : IDisposable
{
    int SaveChanges();
    Task<int> SaveChangesAsync();
    void BeginTransaction();
    void Commit();
    Task CommitAsync();
    void Rollback();
    Task RollbackAsync();
    public IBookRepository BookRepository { get; }
    public ICartRepository CartRepository { get; }
    public ICartItemRepository CartItemRepository { get; }
    public IOrderRepository OrderRepository { get; }
    public IOrderItemRepository OrderItemRepository { get; }
    public IUserRepository UserRepository { get; }
    public IUserCredentialRepository UserCredentialRepository { get; }
}
