﻿using _1.Domain.Entities;

namespace _3.DataAccessLayer.Repositories.IRepositories;

public interface IUserCredentialRepository : IGenericRepository<UserCredential>
{
    Task<UserCredential> Find(string username);
    Task<bool> CheckExistUser(string username);
}
