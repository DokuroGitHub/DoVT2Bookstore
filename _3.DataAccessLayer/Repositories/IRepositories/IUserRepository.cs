﻿using _1.Domain.Entities;

namespace _3.DataAccessLayer.Repositories.IRepositories;

public interface IUserRepository : IGenericRepository<User>
{
    Task<User> Find(string email);
    Task<bool> CheckExistUser(string email);
}
