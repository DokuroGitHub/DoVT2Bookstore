using _1.Domain.Entities;

namespace _3.DataAccessLayer.Repositories.IRepositories;

public interface ICartRepository : IGenericRepository<Cart>
{

}
