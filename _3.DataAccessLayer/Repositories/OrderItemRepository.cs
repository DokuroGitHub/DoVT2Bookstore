using _1.Domain.Entities;
using _2.Domain2Db;
using _3.DataAccessLayer.Repositories.IRepositories;
using _3.DataAccessLayer.Services.IServices;

namespace _3.DataAccessLayer.Repositories;

public class OrderItemRepository : GenericRepository<OrderItem>, IOrderItemRepository
{
    private readonly ApplicationDbContext _context;
    private readonly ICurrentTimeService _currentTimeService;
    private readonly ICurrentUserService _currentUserService;

    public OrderItemRepository(
        ApplicationDbContext context,
        ICurrentTimeService currentTimeService,
        ICurrentUserService currentUserService)
        : base(
            context,
            currentTimeService,
            currentUserService)
    {
        _context = context;
        _currentTimeService = currentTimeService;
        _currentUserService = currentUserService;
    }
}
