﻿using _1.Domain.Entities;
using _2.Domain2Db;
using _3.DataAccessLayer.Commons;
using _3.DataAccessLayer.Repositories.IRepositories;
using _3.DataAccessLayer.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace _3.DataAccessLayer.Repositories;

public class UserCredentialRepository : GenericRepository<UserCredential>, IUserCredentialRepository
{
    private readonly ApplicationDbContext _context;

    public UserCredentialRepository(
        ApplicationDbContext context,
        ICurrentTimeService currentTimeService,
        ICurrentUserService currentUserService)
        : base(
            context,
            currentTimeService,
            currentUserService)
    {
        _context = context;
    }

    public async Task<bool> CheckExistUser(string email) => await _context.Users.AnyAsync(x => x.Email == email);

    public async Task<UserCredential> Find(string username)
    {
        var userCredential = await _context.UserCredentials.FirstOrDefaultAsync(x => x.Username == username);
        if (userCredential == null)
        {
            throw new RepositoryException("Incorrect username!!!");
        }
        return userCredential;
    }
}
