﻿using _1.Domain.Entities;
using _2.Domain2Db;
using _3.DataAccessLayer.Commons;
using _3.DataAccessLayer.Repositories.IRepositories;
using _3.DataAccessLayer.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace _3.DataAccessLayer.Repositories;

public class UserRepository : GenericRepository<User>, IUserRepository
{
    private readonly ApplicationDbContext _context;

    public UserRepository(
        ApplicationDbContext context,
        ICurrentTimeService currentTimeService,
        ICurrentUserService currentUserService)
        : base(
            context,
            currentTimeService,
            currentUserService)
    {
        _context = context;
    }

    public async Task<bool> CheckExistUser(string email) => await _context.Users.AnyAsync(x => x.Email == email);

    public async Task<User> Find(string email)
    {
        var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
        if (user == null)
        {
            throw new RepositoryException("Incorrect Email!!!");
        }
        return user;
    }
}
