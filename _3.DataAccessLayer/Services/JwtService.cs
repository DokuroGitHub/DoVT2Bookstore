using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using _3.Share.ViewModels;
using _3.DataAccessLayer.Services.IServices;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using _3.Share;

namespace _3.DataAccessLayer.Services;

public class JwtService : IJwtService
{
    private readonly Appsettings _appSettings;

    public JwtService(Appsettings appSettings)
    {
        _appSettings = appSettings;
    }

    public string GenerateJWT(CurrentUser user)
    {
        var issuer = _appSettings.Jwt.Issuer;
        var audience = _appSettings.Jwt.Audience;
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Jwt.Key!));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
        var claims = new[]
        {
            new Claim("ID", user.UserId),
            new Claim(ClaimTypes.NameIdentifier, user.UserId),
            new Claim("Email", user.Email),
            new Claim(JwtRegisteredClaimNames.Email, user.Email),
            new Claim("Role", user.Role),
            new Claim(ClaimTypes.Role, user.Role),
        };
        var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                audience: audience,
                issuer: issuer,
                signingCredentials: credentials
            );

        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    public ClaimsPrincipal Validate(string token)
    {
        IdentityModelEventSource.ShowPII = true;
        TokenValidationParameters validationParameters = new()
        {
            ValidateLifetime = true,
            ValidAudience = _appSettings.Jwt.Audience,
            ValidIssuer = _appSettings.Jwt.Issuer,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.Jwt.Key))
        };

        var principal = new JwtSecurityTokenHandler().ValidateToken(token, validationParameters, out _);

        return principal;
    }

    public string Hash(string inputString) => BCrypt.Net.BCrypt.HashPassword(inputString);

    public bool Verify(string password, string hashPassword) => BCrypt.Net.BCrypt.Verify(password, hashPassword);
}
