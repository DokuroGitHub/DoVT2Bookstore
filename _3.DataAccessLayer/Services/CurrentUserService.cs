﻿using _3.Share.Commons;
using _3.DataAccessLayer.Services.IServices;
using Microsoft.AspNetCore.Http;

namespace _3.DataAccessLayer.Services;

public class CurrentUserService : ICurrentUserService
{
    private readonly IJwtService _jwtService;
    private string? accessToken;

    public CurrentUserService(IHttpContextAccessor httpContextAccessor, IJwtService jwtService)
    {
        _jwtService = jwtService;
        accessToken = httpContextAccessor?.HttpContext?.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").LastOrDefault();
    }
    public string CurrentUserId
    {
        get
        {
            if (accessToken == null)
                throw new ServiceException("No access token found!!!");
            var id = _jwtService.Validate(accessToken).Claims.FirstOrDefault(c => c.Type == "ID")?.Value;
            if (id == null)
                throw new ServiceException("No user id found!!!");
            return id;
        }
    }

    public bool IsAdmin
    {
        get
        {
            if (accessToken == null)
                throw new ServiceException("No access token found!!!");
            var role = _jwtService.Validate(accessToken).Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
            if (role == null)
                throw new ServiceException("No role found!!!");
            return role == "Admin";
        }
    }
}
