﻿using _3.DataAccessLayer.Services.IServices;

namespace _3.DataAccessLayer.Services;

public class CurrentTimeService : ICurrentTimeService
{
    public DateTime GetCurrentTime() => DateTime.UtcNow;
}
