﻿namespace _3.DataAccessLayer.Services.IServices;

public interface ICurrentTimeService
{
    public DateTime GetCurrentTime();
}
