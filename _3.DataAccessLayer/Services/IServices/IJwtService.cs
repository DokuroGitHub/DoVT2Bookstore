using System.Security.Claims;
using _3.Share.ViewModels;

namespace _3.DataAccessLayer.Services.IServices;

public interface IJwtService
{
    string GenerateJWT(CurrentUser user);
    ClaimsPrincipal Validate(string token);
    string Hash(string input);
    bool Verify(string password, string hashPassword);
}
