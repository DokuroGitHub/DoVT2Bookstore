﻿namespace _3.DataAccessLayer.Services.IServices;

public interface ICurrentUserService
{
    public string CurrentUserId { get; }
    public bool IsAdmin { get; }
}
