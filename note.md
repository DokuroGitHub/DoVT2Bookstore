# note

```js
//* AdvancedSearch
// filter
{
   Param_0 => (
      (
         (
            (
               (
                  True AndAlso (
                     (Param_0.Title != null) AndAlso Param_0.Title.Contains(value.title)
                  )
               ) AndAlso (
                  (Param_0.Description != null) AndAlso Param_0.Description.Contains(value.description)
               )
            ) AndAlso (
               (Param_0.Author != null) AndAlso Param_0.Author.Contains(value.author)
            )
         ) AndAlso (
            (Param_0.Genre != null) AndAlso Param_0.Genre.Contains(value.genre)
         )
      ) AndAlso (Param_0.Status == value.status)
   )
}

{
   Param_0 => (            
      True
      AndAlso (Param_0.Title != null) AndAlso Param_0.Title.Contains(value.title)
      AndAlso (Param_0.Description != null) AndAlso Param_0.Description.Contains(value.description)
      AndAlso (Param_0.Author != null) AndAlso Param_0.Author.Contains(value.author)
      AndAlso (Param_0.Genre != null) AndAlso Param_0.Genre.Contains(value.genre)
      AndAlso (Param_0.Status == value.status)
   )
}
// include
{System.Func<System.Linq.IQueryable<Domain.Entities.Book>, System.Linq.IQueryable<Domain.Entities.Book>>}

```
