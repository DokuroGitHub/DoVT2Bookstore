﻿using Infrastructure.Repositories.IRepositories;

namespace Infrastructure;

public interface IUnitOfWork : IDisposable
{
    int SaveChanges();
    Task<int> SaveChangesAsync();
    void BeginTransaction();
    void Commit();
    Task CommitAsync();
    void Rollback();
    Task RollbackAsync();
    public IBookRepository BookRepository { get; }
    public ICartRepository CartRepository { get; }
    public IUserRepository UserRepository { get; }
    public IUserCredentialRepository UserCredentialRepository { get; }
}
