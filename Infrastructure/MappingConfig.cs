﻿using Application.ViewModels;
using Application.ViewModels.Book;
using Application.ViewModels.Cart;
using Application.ViewModels.CartItem;
using Application.ViewModels.Order;
using Application.ViewModels.OrderEvent;
using Application.ViewModels.OrderItem;
using Application.ViewModels.Review;
using Application.ViewModels.User;
using Application.ViewModels.UserCredential;
using Application.ViewModels.Wishlist;
using Application.ViewModels.WishlistItem;
using AutoMapper;
using Domain.Entities;

namespace Infrastructure;

public class MappingConfig : Profile
{
    public MappingConfig()
    {
        // PagedResult
        CreateMap(typeof(PagedResult<>), typeof(PagedResult<>));

        // Book
        CreateMap<Book, BookDto>().MaxDepth(2).ReverseMap();
        CreateMap<Book, BookCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Book, BookUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Book, BookFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<Book, BookFlatRefDto>().MaxDepth(2).ReverseMap();

        // Cart
        CreateMap<Cart, CartDto>().MaxDepth(2).ReverseMap();
        CreateMap<Cart, CartCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Cart, CartUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Cart, CartFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<Cart, CartFlatRefDto>().MaxDepth(2).ReverseMap();

        // CartItem
        CreateMap<CartItem, CartItemDto>().MaxDepth(2).ReverseMap();
        CreateMap<CartItem, CartItemCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<CartItem, CartItemUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<CartItem, CartItemFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<CartItem, CartItemFlatRefDto>().MaxDepth(2).ReverseMap();

        // Order
        CreateMap<Order, OrderDto>().MaxDepth(2).ReverseMap();
        CreateMap<Order, OrderCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Order, OrderUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Order, OrderFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<Order, OrderFlatRefDto>().MaxDepth(2).ReverseMap();

        // OrderEvent
        CreateMap<OrderEvent, OrderEventDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderEvent, OrderEventCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderEvent, OrderEventFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderEvent, OrderEventFlatRefDto>().MaxDepth(2).ReverseMap();

        // OrderItem
        CreateMap<OrderItem, OrderItemDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderItem, OrderItemCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderItem, OrderItemUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderItem, OrderItemFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<OrderItem, OrderItemFlatRefDto>().MaxDepth(2).ReverseMap();

        // Review
        CreateMap<Review, ReviewDto>().MaxDepth(2).ReverseMap();
        CreateMap<Review, ReviewCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Review, ReviewUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Review, ReviewFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<Review, ReviewFlatRefDto>().MaxDepth(2).ReverseMap();

        // User
        CreateMap<User, UserDto>().MaxDepth(2).ReverseMap();
        CreateMap<User, UserCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<User, UserUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<User, UserFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<User, UserFlatRefDto>().MaxDepth(2).ReverseMap();

        // UserCredential
        CreateMap<UserCredential, UserCredentialDto>().MaxDepth(2).ReverseMap();
        CreateMap<UserCredential, UserCredentialCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<UserCredential, UserCredentialUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<UserCredential, UserCredentialFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<UserCredential, UserCredentialFlatRefDto>().MaxDepth(2).ReverseMap();

        // Wishlist
        CreateMap<Wishlist, WishlistDto>().MaxDepth(2).ReverseMap();
        CreateMap<Wishlist, WishlistCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Wishlist, WishlistUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<Wishlist, WishlistFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<Wishlist, WishlistFlatRefDto>().MaxDepth(2).ReverseMap();

        // WishlistItem
        CreateMap<WishlistItem, WishlistItemDto>().MaxDepth(2).ReverseMap();
        CreateMap<WishlistItem, WishlistItemCreateDto>().MaxDepth(2).ReverseMap();
        CreateMap<WishlistItem, WishlistItemUpdateDto>().MaxDepth(2).ReverseMap();
        CreateMap<WishlistItem, WishlistItemFlatDto>().MaxDepth(2).ReverseMap();
        CreateMap<WishlistItem, WishlistItemFlatRefDto>().MaxDepth(2).ReverseMap();
    }
}
