# Infrastructure

```bash
dotnet new classlib

dotnet add reference "../Domain2Db/Domain2Db.csproj"
dotnet add reference "../Application/Application.csproj"
#
dotnet build
#
dotnet add package AutoMapper
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package BCrypt.Net-Next
#
```
