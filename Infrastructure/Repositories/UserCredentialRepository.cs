﻿using Domain.Entities;
using Domain2Db;
using Infrastructure.Commons;
using Infrastructure.Repositories.IRepositories;
using Infrastructure.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class UserCredentialRepository : GenericRepository<UserCredential>, IUserCredentialRepository
    {
        private readonly ApplicationDbContext _context;

        public UserCredentialRepository(
            ApplicationDbContext context,
            ICurrentTimeService currentTimeService,
            ICurrentUserService currentUserService)
            : base(
                context,
                currentTimeService,
                currentUserService)
        {
            _context = context;
        }

        public async Task<bool> CheckExistUser(string email) => await _context.Users.AnyAsync(x => x.Email == email);

        public async Task<UserCredential> Find(string username)
        {
            var userCredential = await _context.UserCredentials.FirstOrDefaultAsync(x => x.Username == username);
            if (userCredential == null)
            {
                throw new RepositoryException("Incorrect username!!!");
            }
            return userCredential;
        }
    }
}
