using Domain.Entities;

namespace Infrastructure.Repositories.IRepositories;

public interface IBookRepository : IGenericRepository<Book>
{
    // IQueryable<Book> AsQueryable();
}
