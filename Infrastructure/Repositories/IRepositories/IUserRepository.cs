﻿using Domain.Entities;

namespace Infrastructure.Repositories.IRepositories;

public interface IUserRepository : IGenericRepository<User>
{
    Task<User> Find(string email);
    Task<bool> CheckExistUser(string email);
}
