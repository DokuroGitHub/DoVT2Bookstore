﻿using Domain.Entities;

namespace Infrastructure.Repositories.IRepositories;

public interface IUserCredentialRepository : IGenericRepository<UserCredential>
{
    Task<UserCredential> Find(string username);
    Task<bool> CheckExistUser(string username);
}
