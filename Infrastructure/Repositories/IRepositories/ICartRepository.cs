using Domain.Entities;

namespace Infrastructure.Repositories.IRepositories;

public interface ICartRepository : IGenericRepository<Cart>
{

}
