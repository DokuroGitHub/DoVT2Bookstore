﻿using System.Linq.Expressions;
using Application.ViewModels;

namespace Infrastructure.Repositories.IRepositories;

public interface IGenericRepository<TEntity>
{
    void Add(TEntity entity);
    Task AddAsync(TEntity entity);
    void AddRange(IEnumerable<TEntity> entities);
    Task AddRangeAsync(IEnumerable<TEntity> entities);
    bool Any(Expression<Func<TEntity, bool>>? filter = null);
    Task<bool> AnyAsync(Expression<Func<TEntity, bool>>? filter = null);
    int Count(Expression<Func<TEntity, bool>>? filter = null);
    Task<int> CountAsync(Expression<Func<TEntity, bool>>? filter = null);
    double LongCount(Expression<Func<TEntity, bool>>? filter = null);
    Task<double> LongCountAsync(Expression<Func<TEntity, bool>>? filter = null);
    PagedResult<TEntity> GetPagedResult(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false,
        int pageIndex = 0,
        int pageSize = 10);
    Task<PagedResult<TEntity>> GetPagedResultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false,
        int pageIndex = 0,
        int pageSize = 10);
    IEnumerable<TEntity> GetAll(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false);
    Task<IEnumerable<TEntity>> GetAllAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false);
    TEntity? FirstOrdDefault(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false);
    Task<TEntity?> FirstOrDefaultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false);
    void Update(TEntity entity);
    void UpdateRange(IEnumerable<TEntity> entities);
    void Remove(TEntity entity);
    void Remove(Expression<Func<TEntity, bool>> filter);
    Task RemoveAsync(Expression<Func<TEntity, bool>> filter);
    void RemoveRange(IEnumerable<TEntity> entities);
    void RemoveRange(Expression<Func<TEntity, bool>> filter);
    Task RemoveRangeAsync(Expression<Func<TEntity, bool>> filter);
    void SoftRemove(TEntity entity);
    Task SoftRemoveAsync(Expression<Func<TEntity, bool>> filter);
    void SoftRemoveRange(IEnumerable<TEntity> entities);
    void SoftRemoveRange(Expression<Func<TEntity, bool>> filter);
    Task SoftRemoveRangeAsync(Expression<Func<TEntity, bool>> filter);
}
