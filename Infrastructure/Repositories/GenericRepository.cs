﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Infrastructure.Repositories.IRepositories;
using Domain.Entities;
using Domain2Db;
using Application.ViewModels;
using Infrastructure.Services.IServices;
using Infrastructure.Commons;

namespace Infrastructure.Repositories;

public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
{
    protected DbSet<TEntity> _dbSet;
    private readonly ICurrentTimeService _currentTimeService;
    private readonly ICurrentUserService _currentUserService;
    //
    public GenericRepository(
        ApplicationDbContext context,
        ICurrentTimeService currentTimeService,
        ICurrentUserService currentUserService)
    {
        _dbSet = context.Set<TEntity>();
        _currentTimeService = currentTimeService;
        _currentUserService = currentUserService;
    }
    //
    public void Add(TEntity entity)
    {
        entity.CreatedAt = _currentTimeService.GetCurrentTime();
        entity.CreatedBy = _currentUserService.CurrentUserId;
        _dbSet.Add(entity);
    }

    public async Task AddAsync(TEntity entity)
    {
        entity.CreatedAt = _currentTimeService.GetCurrentTime();
        entity.CreatedBy = _currentUserService.CurrentUserId;
        await _dbSet.AddAsync(entity);
    }

    public void AddRange(IEnumerable<TEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.CreatedAt = _currentTimeService.GetCurrentTime();
            entity.CreatedBy = _currentUserService.CurrentUserId;
        }
        _dbSet.AddRange(entities);
    }

    public async Task AddRangeAsync(IEnumerable<TEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.CreatedAt = _currentTimeService.GetCurrentTime();
            entity.CreatedBy = _currentUserService.CurrentUserId;
        }
        await _dbSet.AddRangeAsync(entities);
    }

    public bool Any(Expression<Func<TEntity, bool>>? filter = null)
    {
        if (filter != null)
            return _dbSet.Any(filter);
        return _dbSet.Any();
    }

    public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>>? filter = null)
    {
        if (filter != null)
            return await _dbSet.AnyAsync(filter);
        return await _dbSet.AnyAsync();
    }

    public int Count(Expression<Func<TEntity, bool>>? filter = null)
    {
        if (filter == null)
            return _dbSet.Count();
        return _dbSet.Count(filter);
    }

    public async Task<int> CountAsync(Expression<Func<TEntity, bool>>? filter = null)
    {
        if (filter == null)
            return await _dbSet.CountAsync();
        return await _dbSet.CountAsync(filter);
    }

    public double LongCount(Expression<Func<TEntity, bool>>? filter = null)
    {
        if (filter == null)
            return _dbSet.LongCount();
        return _dbSet.LongCount(filter);
    }

    public async Task<double> LongCountAsync(Expression<Func<TEntity, bool>>? filter = null)
    {
        if (filter == null)
            return await _dbSet.LongCountAsync();
        return await _dbSet.LongCountAsync(filter);
    }

    public PagedResult<TEntity> GetPagedResult(
           Expression<Func<TEntity, bool>>? filter = null,
           Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
           Expression<Func<TEntity, object>>? orderBy = null,
           bool orderByDescending = false,
           int pageIndex = 1,
           int pageSize = 10)
    {
        var query = _dbSet.AsQueryable();

        // Apply filter if it is provided
        if (filter != null)
        {
            query = query.Where(filter);
        }

        // Apply include if it is provided
        if (include != null)
        {
            query = include(query);
        }

        // Apply orderBy if it is provided
        if (orderBy != null)
        {
            query = orderByDescending
                ? query.OrderByDescending(orderBy)
                : query.OrderBy(orderBy);
        }

        var count = query.Count();
        query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        List<TEntity> items = query.ToList();

        var result = new PagedResult<TEntity>(
            items: items,
            count: count,
            pageIndex: pageIndex,
            pageSize: pageSize
        );
        return result;
    }

    public async Task<PagedResult<TEntity>> GetPagedResultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false,
        int pageIndex = 1,
        int pageSize = 10)
    {
        var query = _dbSet.AsQueryable();

        // Apply filter if it is provided
        if (filter != null)
        {
            query = query.Where(filter);
        }

        // Apply include if it is provided
        if (include != null)
        {
            query = include(query);
        }

        // Apply orderBy if it is provided
        if (orderBy != null)
        {
            query = orderByDescending
                ? query.OrderByDescending(orderBy)
                : query.OrderBy(orderBy);
        }

        var count = await query.CountAsync();
        query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        List<TEntity> items = await query.ToListAsync();

        var result = new PagedResult<TEntity>(
            items: items,
            count: count,
            pageIndex: pageIndex,
            pageSize: pageSize
        );
        return result;
    }

    public IEnumerable<TEntity> GetAll(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false)
    {
        var query = _dbSet.AsQueryable();

        // Apply filter if it is provided
        if (filter != null)
        {
            query = query.Where(filter);
        }

        // Apply include if it is provided
        if (include != null)
        {
            query = include(query);
        }

        // Apply orderBy if it is provided
        if (orderBy != null)
        {
            query = orderByDescending
                ? query.OrderByDescending(orderBy)
                : query.OrderBy(orderBy);
        }

        var count = query.Count();
        var items = query.ToList();

        return items;
    }

    public async Task<IEnumerable<TEntity>> GetAllAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false)
    {
        var query = _dbSet.AsQueryable();

        // Apply filter if it is provided
        if (filter != null)
        {
            query = query.Where(filter);
        }

        // Apply include if it is provided
        if (include != null)
        {
            query = include(query);
        }

        // Apply orderBy if it is provided
        if (orderBy != null)
        {
            query = orderByDescending
                ? query.OrderByDescending(orderBy)
                : query.OrderBy(orderBy);
        }

        var count = await query.CountAsync();
        var items = await query.ToListAsync();

        return items;
    }

    public TEntity? FirstOrdDefault(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false)
    {
        var query = _dbSet.AsQueryable().IgnoreQueryFilters().AsNoTracking();

        // Apply filter if it is provided
        if (filter != null)
        {
            query = query.Where(filter);
        }

        // Apply include if it is provided
        if (include != null)
        {
            query = include(query);
        }

        // Apply orderBy if it is provided
        if (orderBy != null)
        {
            query = orderByDescending
                ? query.OrderByDescending(orderBy)
                : query.OrderBy(orderBy);
        }

        var item = filter != null ? query.FirstOrDefault(filter)
            : query.FirstOrDefault();

        return item;
    }

    public async Task<TEntity?> FirstOrDefaultAsync(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IQueryable<TEntity>>? include = null,
        Expression<Func<TEntity, object>>? orderBy = null,
        bool orderByDescending = false)
    {
        var query = _dbSet.AsQueryable().IgnoreQueryFilters().AsNoTracking();

        // Apply filter if it is provided
        if (filter != null)
        {
            query = query.Where(filter);
        }

        // Apply include if it is provided
        if (include != null)
        {
            query = include(query);
        }

        // Apply orderBy if it is provided
        if (orderBy != null)
        {
            query = orderByDescending
                ? query.OrderByDescending(orderBy)
                : query.OrderBy(orderBy);
        }

        var item = filter != null ? await query.FirstOrDefaultAsync(filter)
            : await query.FirstOrDefaultAsync();

        return item;
    }

    public void Update(TEntity entity)
    {
        if (entity == null)
            throw new RepositoryException("Entity null");
        entity.UpdatedAt = _currentTimeService.GetCurrentTime();
        entity.UpdatedBy = _currentUserService.CurrentUserId;
        _dbSet.Update(entity);
    }

    public void UpdateRange(IEnumerable<TEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.UpdatedAt = _currentTimeService.GetCurrentTime();
            entity.UpdatedBy = _currentUserService.CurrentUserId;
        }
        _dbSet.UpdateRange(entities);
    }

    public void Remove(TEntity entity)
    {
        if (entity == null)
            throw new RepositoryException("Entity null");
        _dbSet.Remove(entity);
    }

    public void Remove(Expression<Func<TEntity, bool>> filter)
    {
        var entity = _dbSet.FirstOrDefault(filter);
        if (entity == null)
            throw new RepositoryException("Entity not found");
        _dbSet.Remove(entity);
    }

    public async Task RemoveAsync(Expression<Func<TEntity, bool>> filter)
    {
        var entity = await _dbSet.FirstOrDefaultAsync(filter);
        if (entity == null)
            throw new RepositoryException("Entity not found");
        _dbSet.Remove(entity);
    }

    public void RemoveRange(IEnumerable<TEntity> entities)
    {
        if (entities == null)
            throw new RepositoryException("Entities not found");
        _dbSet.RemoveRange(entities);
    }

    public void RemoveRange(Expression<Func<TEntity, bool>> filter)
    {
        var entities = GetAll(filter);
        if (entities == null)
            throw new RepositoryException("Entities not found");
        _dbSet.RemoveRange(entities);
    }

    public async Task RemoveRangeAsync(Expression<Func<TEntity, bool>> filter)
    {
        var entities = await GetAllAsync(filter);
        if (entities == null)
            throw new RepositoryException("Entities not found");
        _dbSet.RemoveRange(entities);
    }

    public void SoftRemove(TEntity entity)
    {
        if (entity == null)
            throw new RepositoryException("Entity null");
        entity.DeletedAt = _currentTimeService.GetCurrentTime();
        entity.DeletedBy = _currentUserService.CurrentUserId;
        _dbSet.Update(entity);
    }

    public void SoftRemove(Expression<Func<TEntity, bool>> filter)
    {
        var entity = FirstOrdDefault(filter);
        if (entity == null)
            throw new RepositoryException("Entity null");
        entity.DeletedAt = _currentTimeService.GetCurrentTime();
        entity.DeletedBy = _currentUserService.CurrentUserId;
        _dbSet.UpdateRange(entity);
    }

    public async Task SoftRemoveAsync(Expression<Func<TEntity, bool>> filter)
    {
        var entity = await FirstOrDefaultAsync(filter);
        if (entity == null)
            throw new RepositoryException("Entity null");
        entity.DeletedAt = _currentTimeService.GetCurrentTime();
        entity.DeletedBy = _currentUserService.CurrentUserId;
        _dbSet.UpdateRange(entity);
    }

    public void SoftRemoveRange(IEnumerable<TEntity> entities)
    {
        if (entities == null)
            throw new RepositoryException("Entities null");
        foreach (var entity in entities)
        {
            if (entity == null)
                throw new RepositoryException("Entity null");
            entity.DeletedAt = _currentTimeService.GetCurrentTime();
            entity.DeletedBy = _currentUserService.CurrentUserId;
        }
        _dbSet.UpdateRange(entities);
    }

    public void SoftRemoveRange(Expression<Func<TEntity, bool>> filter)
    {
        var entities = GetAll(filter);
        if (entities == null)
            throw new RepositoryException("Entities not found");
        foreach (var entity in entities)
        {
            if (entity == null)
                throw new RepositoryException("Entity null");
            entity.DeletedAt = _currentTimeService.GetCurrentTime();
            entity.DeletedBy = _currentUserService.CurrentUserId;
        }
        _dbSet.UpdateRange(entities);
    }

    public async Task SoftRemoveRangeAsync(Expression<Func<TEntity, bool>> filter)
    {
        var entities = await GetAllAsync(filter);
        if (entities == null)
            throw new RepositoryException("Entities not found");
        foreach (var entity in entities)
        {
            if (entity == null)
                throw new RepositoryException("Entity null");
            entity.DeletedAt = _currentTimeService.GetCurrentTime();
            entity.DeletedBy = _currentUserService.CurrentUserId;
        }
        _dbSet.UpdateRange(entities);
    }
}
