﻿using Domain.Entities;
using Domain2Db;
using Infrastructure.Commons;
using Infrastructure.Repositories.IRepositories;
using Infrastructure.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly ApplicationDbContext _context;

        public UserRepository(
            ApplicationDbContext context,
            ICurrentTimeService currentTimeService,
            ICurrentUserService currentUserService)
            : base(
                context,
                currentTimeService,
                currentUserService)
        {
            _context = context;
        }

        public async Task<bool> CheckExistUser(string email) => await _context.Users.AnyAsync(x => x.Email == email);

        public async Task<User> Find(string email)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
            if (user == null)
            {
                throw new RepositoryException("Incorrect Email!!!");
            }
            return user;
        }
    }
}
