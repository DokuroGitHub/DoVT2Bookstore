using Domain.Entities;
using Domain2Db;
using Infrastructure.Repositories.IRepositories;
using Infrastructure.Services.IServices;

namespace Infrastructure.Repositories
{
    public class CartRepository : GenericRepository<Cart>, ICartRepository
    {
        private readonly ApplicationDbContext _context;

        public CartRepository(
            ApplicationDbContext context,
            ICurrentTimeService currentTimeService,
            ICurrentUserService currentUserService)
            : base(
                context,
                currentTimeService,
                currentUserService)
        {
            _context = context;
        }
    }
}
