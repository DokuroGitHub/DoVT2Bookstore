using Domain.Entities;
using Domain2Db;
using Infrastructure.Repositories.IRepositories;
using Infrastructure.Services.IServices;

namespace Infrastructure.Repositories;

public class BookRepository : GenericRepository<Book>, IBookRepository
{
    private readonly ApplicationDbContext _context;

    public BookRepository(
        ApplicationDbContext context,
        ICurrentTimeService currentTimeService,
        ICurrentUserService currentUserService)
        : base(
            context,
            currentTimeService,
            currentUserService)
    {
        _context = context;
    }

    // public IQueryable<Book> AsQueryable()
    // {
    //     return _context.Set<Book>().AsQueryable();
    // }
}
