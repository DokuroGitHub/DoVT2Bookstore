namespace Infrastructure;

#pragma warning disable
public class Appsettings
{
    public ConnectionStrings ConnectionStrings { get; set; }
    public MailSettings MailSettings { get; set; }
    public MsGraph MsGraph { get; set; }
    public Jwt Jwt { get; set; }
}

public class ConnectionStrings
{
    public string DokuroSQLConnectionString { get; set; }
}

public class MailSettings
{
    public string Mail { get; set; }
    public string DisplayName { get; set; }
    public string Password { get; set; }
    public string Host { get; set; }
    public string Port { get; set; }
}

public class MsGraph
{
    public string AccessToken { get; set; }
}

public class Jwt
{
    public string Key { get; set; }
    public string Issuer { get; set; }
    public string Audience { get; set; }
}
