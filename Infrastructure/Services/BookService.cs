using Application.IServices;
using Application.Commons;
using Application.ViewModels;
using Application.ViewModels.Book;
using AutoMapper;
using Domain.Entities;
using Infrastructure.Services.IServices;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Infrastructure.Commons;

namespace Infrastructure.Services;

public class BookService : IBookService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IMailService _emailService;

    public BookService(
        IUnitOfWork unitOfWork,
        IMailService emailService,
        IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _emailService = emailService;
        _mapper = mapper;
    }

    public async Task<List<BookDto>> GetAll(Expression<Func<BookDto, object>> parameters)
    {
        //TODO: return only the parameters
        var items = await _unitOfWork.BookRepository.GetAllAsync();
        var result = _mapper.Map<List<BookDto>>(items);
        return result;
    }

    public async Task<PagedResult<BookDto>> GetPagedItems(
        string? search = null,
        string? includeProperties = null,
        int pageIndex = 1,
        int pageSize = 10)
    {
        var result = await _unitOfWork.BookRepository.GetPagedResultAsync(
            filter: x => string.IsNullOrEmpty(search) ? true :
                (x.Title != null && x.Title.Contains(search)) ||
                (x.Description != null && x.Description.Contains(search)) ||
                (x.Author != null && x.Author.Contains(search)),
            include: x => x.Include(x => x.Reviews).ThenInclude(x => x.User)
                .Include(x => x.Reviews).ThenInclude(x => x.OrderItem),
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<BookDto>>(result);
        if (pagedItems == null)
            throw new ServiceException("No books found!!!");
        return pagedItems;
    }

    public async Task<PagedResult<BookDto>> AdvancedSearch(
        string? title = null,
        string? description = null,
        string? author = null,
        string? genre = null,
        string? status = null,
        bool includeReviews = false,
        bool includeReviewsUser = false,
        bool includeOrderItems = false,
        bool includeOrderItemsOrder = false,
        bool includeOrderItemsReview = false,
        int pageIndex = 1,
        int pageSize = 10)
    {
        Expression<Func<Book, bool>> filter = x => true;
        filter = string.IsNullOrEmpty(title) ?
            filter :
            filter.AndAlso(x => x.Title != null && x.Title.Contains(title));

        filter = string.IsNullOrEmpty(description) ?
            filter :
            filter.AndAlso(x => x.Description != null && x.Description.Contains(description));

        filter = string.IsNullOrEmpty(author) ?
            filter :
            filter.AndAlso(x => x.Author != null && x.Author.Contains(author));

        filter = string.IsNullOrEmpty(genre) ?
            filter :
            filter.AndAlso(x => x.Genre != null && x.Genre.Contains(genre));

        filter = string.IsNullOrEmpty(status) ?
            filter :
            filter.AndAlso(x => x.Status == status);

        // Func<IQueryable<Book>, IQueryable<Book>> include = x => x;
        // if (includeReviews)
        //     include = include == null ?
        //         x => x.Include(x => x.Reviews) :
        //         x => include(x).Include(x => x.Reviews);
        // include = x => x.Include(x => x.Reviews);
        // if (includeReviewsUser)
        //     include = include == null ?
        //         x => x.Include(x => x.Reviews).ThenInclude(x => x.User) :
        //         x => include(x).Include(x => x.Reviews).ThenInclude(x => x.User);
        // if (includeOrderItems)
        //     include = include == null ?
        //         x => x.Include(x => x.OrderItems) :
        //         x => include(x).Include(x => x.OrderItems);
        // if (includeOrderItemsOrder)
        //     include = include == null ?
        //         x => x.Include(x => x.OrderItems).ThenInclude(x => x.Order) :
        //         x => include(x).Include(x => x.OrderItems).ThenInclude(x => x.Order);
        // if (includeOrderItemsReview)
        //     include = include == null ?
        //         x => x.Include(x => x.OrderItems).ThenInclude(x => x.Review) :
        //         x => include(x).Include(x => x.OrderItems).ThenInclude(x => x.Review);

        Func<IQueryable<Book>, IQueryable<Book>> include = x =>
        {
            if (includeReviews)
            {
                x = x.Include(x => x.Reviews);
                if (includeReviewsUser)
                {
                    x = x.Include(x => x.Reviews).ThenInclude(x => x.User);
                }
            }

            if (includeOrderItems)
            {
                x = x.Include(x => x.OrderItems);
                if (includeOrderItemsOrder)
                {
                    x = x.Include(x => x.OrderItems).ThenInclude(x => x.Order);
                }
                if (includeOrderItemsReview)
                {
                    x = x.Include(x => x.OrderItems).ThenInclude(x => x.Review);
                }
            }

            return x;
        };

        var items = await _unitOfWork.BookRepository.GetPagedResultAsync(
            filter: filter,
            include: include,
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<BookDto>>(items);
        return pagedItems;
    }

    public async Task<PagedResult<BookDto>> Search(string? search, int pageIndex = 1, int pageSize = 10)
    {
        var result = await _unitOfWork.BookRepository.GetPagedResultAsync(
            filter: search != null ? x => (x.Title != null && x.Title.Contains(search))
                || (x.Author != null && x.Author.Contains(search))
                || (x.Genre != null && x.Genre.Contains(search)) : null,
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<BookDto>>(result);
        if (pagedItems == null)
            throw new ServiceException("No books found!!!");
        return pagedItems;
    }

    public async Task<BookDto?> GetOne(string Id)
    {
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id.ToString() == Id);
        var result = _mapper.Map<BookDto>(book);
        return result;
    }

    public async Task<BookDto> Create(BookCreateDto dto)
    {
        var book = _mapper.Map<Book>(dto);
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.BookRepository.AddAsync(book);
            await _unitOfWork.CommitAsync();
            var result = _mapper.Map<BookDto>(book);
            await SendMail(result);
            return result;
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't add book", ex);
        }
    }

    public async Task<BookDto?> Update(string id, BookUpdateDto dto)
    {
        dto.Id = id;
        var item = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (item == null)
            return null;
        item = _mapper.Map<Book>(dto);
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.BookRepository.Update(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't update book", ex);
        }
        var result = _mapper.Map<BookDto>(item);
        return result;
    }

    public async Task<ResponseTypeId> Delete(string Id)
    {
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id.ToString() == Id);
        if (book == null)
            throw new ServiceException("Not found the book");
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.BookRepository.Remove(book);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete book", ex);
        }
        var result = _mapper.Map<BookDto>(book);
        return new ResponseTypeId { id = result.Id };
    }

    // Send mail to all users
    private async Task SendMail(BookDto dto)
    {
        var users = await _unitOfWork.UserRepository.GetAllAsync(x => x.Email != null, include: x => x.Include(y => y.UserCredential));
        if (users == null)
            throw new ServiceException("No users found!!!");
        var emails = new List<string>();
        foreach (var user in users)
        {
            var email = user.Email;
            var role = user.UserCredential.Role;
            if (!string.IsNullOrEmpty(email) && role != "Admin")
                emails.Add(email);
        }
        if (emails.Count == 0)
            throw new ServiceException("No emails found!!!");
        var subject = "New Book Release";
        string content = "<h3>New Book Release:</h3>" +
              $"<p><strong>Title:</strong> {dto.Title}</p>" +
              $"<p><strong>Author:</strong> {dto.Author}</p>" +
              $"<p><strong>Genre:</strong> {dto.Genre}</p>" +
              $"<p><strong>Price:</strong> {dto.Price} Đ</p>" +
              $"<p><strong>Inventory:</strong> {dto.Quantity}</p>" +
              $"<img src=\"{dto.CoverImage}\" alt=\"Book Cover Image\" style=\"max-width: 200px;\" alt=\"{dto.Title}\">" +
              "<p><a href=\"#\">Visit our website</a> to purchase this book and browse our other titles.</p>";

        _emailService.SendMail(emails: emails, subject: subject, content: content);
    }

}
