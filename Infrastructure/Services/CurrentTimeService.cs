﻿using Infrastructure.Services.IServices;

namespace Infrastructure.Services;

public class CurrentTimeService : ICurrentTimeService
{
    public DateTime GetCurrentTime() => DateTime.UtcNow;
}
