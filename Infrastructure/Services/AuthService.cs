﻿using Application.ViewModels;
using Application.ViewModels.User;
using AutoMapper;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Services.IServices;
using Application.IServices;
using Application.Commons;

namespace Infrastructure.Services;

public class AuthService : IAuthService
{
    private readonly Appsettings _appsettings;
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IJwtService _jwtService;

    public AuthService(
        Appsettings appsettings,
        IUnitOfWork unitOfWork,
        IMapper mapper,
       IJwtService jwtService) : base()
    {
        _appsettings = appsettings;
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _jwtService = jwtService;
    }

    public async Task<bool> IsUniqueUserAsync(string username)
    {
        var user = await _unitOfWork.UserCredentialRepository.FirstOrDefaultAsync(x => x.Username == username);
        return user == null;
    }

    public async Task<LoginResponseDto?> LoginAsync(LoginRequestDto loginRequestDTO)
    {
        var userCredential = await _unitOfWork.UserCredentialRepository.FirstOrDefaultAsync(
            u => u.Username == loginRequestDTO.Username,
            include: x => x.Include(u => u.User));

        if (userCredential == null)
            return null;

        bool isValid = _jwtService.Verify(loginRequestDTO.Password, userCredential.HashPassword);
        if (!isValid)
            return null;

        //if user was found generate JWT Token
        var currentUser = new CurrentUser()
        {
            UserId = userCredential.UserId,
            Email = userCredential.User.Email ?? "",
            Role = userCredential.Role
        };
        var token = _jwtService.GenerateJWT(currentUser);

        var loginResponseDto = new LoginResponseDto()
        {
            Token = token,
            CurrentUser = currentUser,
        };
        return loginResponseDto;
    }

    public async Task<UserFlatDto?> RegisterAsync(RegisterationRequestDto dto)
    {
        var user = new User()
        {
            Email = dto.Email,
            UserCredential = new UserCredential()
            {
                Username = dto.Email,
                HashPassword = BCrypt.Net.BCrypt.HashPassword(dto.Password),
                Role = dto.Role ?? "User"
            }
        };
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Error while saving user to database", ex);
        }
        var result = _mapper.Map<UserFlatDto>(user);
        return result;
    }
}
