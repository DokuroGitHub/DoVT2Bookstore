﻿namespace Infrastructure.Services.IServices;

public interface ICurrentTimeService
{
    public DateTime GetCurrentTime();
}
