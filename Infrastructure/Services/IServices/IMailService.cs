﻿namespace Infrastructure.Services.IServices;

public interface IMailService
{
    Task<string> SendMailMsGraph(string email);
    void SendMail(IEnumerable<string> emails, string subject, string content);
    Task SendMailAsync(IEnumerable<string> emails, string subject, string content);
}
