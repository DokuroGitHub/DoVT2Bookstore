﻿namespace Infrastructure.Services.IServices;

public interface ICurrentUserService
{
    public string CurrentUserId { get; }
}
