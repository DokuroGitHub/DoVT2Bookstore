using System.Security.Claims;
using Application.ViewModels;

namespace Infrastructure.Services.IServices;

public interface IJwtService
{
    string GenerateJWT(CurrentUser user);
    ClaimsPrincipal Validate(string token);
    string Hash(string input);
    bool Verify(string password, string hashPassword);
}
