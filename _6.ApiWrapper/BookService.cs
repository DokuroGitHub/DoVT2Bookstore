﻿using System.Linq.Expressions;
using Application.Commons;
using Application.IServices;
using Application.ViewModels;
using Application.ViewModels.Book;
using Newtonsoft.Json;

namespace ApiWrapper;

public class BookService : IBookService
{
    private string _baseUrl { get; set; }
    private HttpClient _client { get; set; }

    public BookService()
    {
        _client = new HttpClient();
        _baseUrl = "https://localhost:7001/api/v1/Books";
    }

    public async Task<List<BookDto>> GetAll(Expression<Func<BookDto, object>> parameters)
    {
        string requestedParametersString = Utilities.GetProps(parameters);
        var response = await _client.GetAsync(_baseUrl + "?fields=" + requestedParametersString);
        if (!response.IsSuccessStatusCode)
            throw new ServiceException("No books found");
        var content = await response.Content.ReadAsStringAsync();
        var items = JsonConvert.DeserializeObject<List<BookDto>>(content);
        if (items == null)
            throw new ServiceException("No books found");
        return items;
    }

    public Task<PagedResult<BookDto>> GetPagedItems(string? search = null, string? includeProperties = null, int pageIndex = 1, int pageSize = 10)
    {
        throw new NotImplementedException();
    }

    public Task<PagedResult<BookDto>> Search(string? search = null, int pageIndex = 1, int pageSize = 10)
    {
        throw new NotImplementedException();
    }

    public Task<PagedResult<BookDto>> AdvancedSearch(string? title = null, string? description = null, string? author = null, string? genre = null, string? status = null, bool includeReviews = false, bool includeReviewsUser = false, bool includeOrderItems = false, bool includeOrderItemsOrder = false, bool includeOrderItemsReview = false, int pageIndex = 1, int pageSize = 10)
    {
        throw new NotImplementedException();
    }

    public Task<BookDto?> GetOne(string id)
    {
        throw new NotImplementedException();
    }

    public Task<BookDto> Create(BookCreateDto dto)
    {
        throw new NotImplementedException();
    }

    public Task<BookDto?> Update(string id, BookUpdateDto dto)
    {
        throw new NotImplementedException();
    }

    public Task<ResponseTypeId> Delete(string id)
    {
        throw new NotImplementedException();
    }
}
