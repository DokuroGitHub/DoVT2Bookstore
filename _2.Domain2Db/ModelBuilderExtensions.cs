using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace _2.Domain2Db;

public static class ModelBuilderExtensions
{
    public static void Seed(this ModelBuilder modelBuilder)
    {
        if (Directory.Exists("Seeds"))
        {
            modelBuilder.GenericSeed<Book>("Seeds/Books.json");
            modelBuilder.GenericSeed<Cart>("Seeds/Carts.json");
            modelBuilder.GenericSeed<CartItem>("Seeds/CartItems.json");
            modelBuilder.GenericSeed<Order>("Seeds/Orders.json");
            modelBuilder.GenericSeed<OrderEvent>("Seeds/OrderEvents.json");
            modelBuilder.GenericSeed<OrderItem>("Seeds/OrderItems.json");
            modelBuilder.GenericSeed<Review>("Seeds/Reviews.json");
            modelBuilder.GenericSeed<User>("Seeds/Users.json");
            modelBuilder.GenericSeed<UserCredential>("Seeds/UserCredentials.json");
            modelBuilder.GenericSeed<Wishlist>("Seeds/Wishlists.json");
            modelBuilder.GenericSeed<WishlistItem>("Seeds/WishlistItems.json");
        }
    }

    public static void GenericSeed<T>(this ModelBuilder modelBuilder, string path) where T : class
    {
        using var r = new StreamReader(path);
        string json = r.ReadToEnd();
        var items = JsonConvert.DeserializeObject<List<T>>(json);
        if (items != null)
        {
            modelBuilder.Entity<T>().HasData(items);
        }
    }
}
