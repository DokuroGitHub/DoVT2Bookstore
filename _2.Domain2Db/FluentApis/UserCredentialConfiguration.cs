﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class UserCredentialConfiguration : IEntityTypeConfiguration<UserCredential>
{
    public void Configure(EntityTypeBuilder<UserCredential> builder)
    {
        builder.ToTable("UserCredential", x =>
        {
            x.HasCheckConstraint("CK_UserCredential_Role", "[Role] = 'Admin' or [Role] = 'User' or [Role] = 'Guest'");
            x.HasCheckConstraint("CK_UserCredential_CreditBalance", "[CreditBalance] >= 0");
        });
        builder
            .HasKey(x => x.UserId)
            .HasName("PK_UserCredential_UserId");
        builder
            .Property(x => x.Username)
            .ValueGeneratedOnAdd();
        builder
            .HasIndex(x => x.Username, "IX_UserCredential_Username")
            .IsUnique();
        builder
            .Property(p => p.Role)
            .HasDefaultValue("User");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.User)
            .WithOne(x => x.UserCredential)
            .HasForeignKey<UserCredential>(x => x.UserId)
            .HasConstraintName("FK_UserCredential_UserId");
    }
}
