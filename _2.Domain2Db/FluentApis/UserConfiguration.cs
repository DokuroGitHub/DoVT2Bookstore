﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class UserConfiguration : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("User");
        builder
            .HasKey(x => x.Id)
            .HasName("PK_User_Id");
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        builder
            .HasIndex(x => x.Email, "IX_User_Email");
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ghost
        builder
            .Property(p => p.DisplayName)
            .HasComputedColumnSql(@"
CASE
    WHEN [FirstName] IS NOT NULL AND [FirstName] <> ''
        THEN CASE
  			WHEN [LastName] IS NOT NULL AND [LastName] <> '' 
       			THEN [FirstName] + ' ' + [LastName]
       		ELSE [FirstName]
  		END
    WHEN [LastName] IS NOT NULL AND [LastName] <> '' THEN [LastName]
    WHEN [Email] IS NOT NULL AND [Email] <> '' THEN [Email]
	ELSE [Id]
END", stored: false);
        // ref
        builder
            .HasOne(x => x.UserCredential)
            .WithOne(x => x.User)
            .HasForeignKey<UserCredential>(x => x.UserId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.Carts)
            .WithOne(x => x.User)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.Orders)
            .WithOne(x => x.User)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.Reviews)
            .WithOne(x => x.User)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.Wishlists)
            .WithOne(x => x.User)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Restrict);
    }
}
