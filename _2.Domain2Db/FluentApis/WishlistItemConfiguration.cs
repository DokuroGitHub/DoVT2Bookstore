﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class WishlistItemConfiguration : IEntityTypeConfiguration<WishlistItem>
{
    public void Configure(EntityTypeBuilder<WishlistItem> builder)
    {
        builder.ToTable("WishlistItem", x => x.HasCheckConstraint("CK_WishlistItem_Quantity", "[Quantity] > 0"));
        builder
            .HasKey(x => new { x.WishlistId, x.BookId })
            .HasName("PK_WishlistItem_WishlistId_BookId");
        builder
            .Property(x => x.Quantity)
            .HasDefaultValue(1);
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.Wishlist)
            .WithMany(x => x.WishlistItems)
            .HasForeignKey(x => x.WishlistId)
            .HasConstraintName("FK_WishlistItem_WishlistId");
        builder
            .HasOne(x => x.Book)
            .WithMany(x => x.WishlistItems)
            .HasForeignKey(x => x.BookId)
            .HasConstraintName("FK_WishlistItem_BookId");
    }
}
