﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class WishlistConfiguration : IEntityTypeConfiguration<Wishlist>
{
    public void Configure(EntityTypeBuilder<Wishlist> builder)
    {
        builder.ToTable("Wishlist", x => x.HasCheckConstraint("CK_Wishlist_ItemCount", "[ItemCount] >= 0"));
        builder
            .HasKey(x => x.Id)
            .HasName("PK_Wishlist_Id");
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.User)
            .WithMany(x => x.Wishlists)
            .HasForeignKey(x => x.UserId)
            .HasConstraintName("FK_Wishlist_UserId");
        builder
            .HasMany(x => x.WishlistItems)
            .WithOne(x => x.Wishlist)
            .HasForeignKey(x => x.WishlistId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
