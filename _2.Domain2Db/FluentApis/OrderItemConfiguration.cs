﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
{
    public void Configure(EntityTypeBuilder<OrderItem> builder)
    {
        builder.ToTable("OrderItem", x => x.HasCheckConstraint("CK_OrderItem_Quantity", "[Quantity] > 0"));
        builder
            .HasKey(x => new { x.OrderId, x.BookId })
            .HasName("PK_OrderItem_OrderId_BookId");
        builder
            .Property(x => x.Quantity)
            .HasDefaultValue(1);
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.Order)
            .WithMany(x => x.OrderItems)
            .HasForeignKey(x => x.OrderId)
            .HasConstraintName("FK_OrderItem_OrderId");
        builder
            .HasOne(x => x.Book)
            .WithMany(x => x.OrderItems)
            .HasForeignKey(x => x.BookId)
            .HasConstraintName("FK_OrderItem_BookId");
        builder
            .HasOne(x => x.Review)
            .WithOne(x => x.OrderItem)
            .HasForeignKey<Review>(x => new { x.OrderId, x.BookId })
            .OnDelete(DeleteBehavior.Restrict);
    }
}
