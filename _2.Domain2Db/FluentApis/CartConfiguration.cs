﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class CartConfiguration : IEntityTypeConfiguration<Cart>
{
    public void Configure(EntityTypeBuilder<Cart> builder)
    {
        builder.ToTable("Cart", x =>
        {
            x.HasCheckConstraint("CK_Cart_OriginalTotalPrice", "[OriginalTotalPrice] >= 0");
            x.HasCheckConstraint("CK_Cart_TotalDiscount", "[TotalDiscount] >= 0");
            x.HasCheckConstraint("CK_Cart_TotalPrice", "[TotalPrice] >= 0");
            x.HasCheckConstraint("CK_Cart_Prices", "[TotalPrice] >= [OriginalTotalPrice] - [TotalDiscount]");
            x.HasCheckConstraint("CK_Cart_ItemCount", "[ItemCount] >= 0");
        });
        builder
            .HasKey(x => x.Id)
            .HasName("PK_Cart_Id");
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        builder
            .Property(x => x.Currency)
            .HasDefaultValue("VND");
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.User)
            .WithMany(x => x.Carts)
            .HasForeignKey(x => x.UserId)
            .HasConstraintName("FK_Cart_UserId");
        builder
            .HasMany(x => x.CartItems)
            .WithOne(x => x.Cart)
            .HasForeignKey(x => x.CartId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
