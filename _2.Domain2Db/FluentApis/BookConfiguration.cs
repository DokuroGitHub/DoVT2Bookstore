﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class BookConfiguration : IEntityTypeConfiguration<Book>
{
    public void Configure(EntityTypeBuilder<Book> builder)
    {
        builder.ToTable("Book", x =>
        {
            x.HasCheckConstraint("CK_Book_Price", "[Price] >= 0");
            x.HasCheckConstraint("CK_Book_Quantity", "[Quantity] >= 0");
            x.HasCheckConstraint("CK_Book_AvgRating", "[AvgRating] >= 0 AND [AvgRating] <= 5");
        });
        builder
            .HasKey(x => x.Id)
            .HasName("PK_Book_Id");
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        builder
            .Property(x => x.Currency)
            .HasDefaultValue("VND");
        builder
            .Property(x => x.PublicationDate)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ghost
        builder
            .Property(p => p.Status)
            .HasComputedColumnSql(@"
CASE
    WHEN [DeletedAt] IS NOT NULL THEN 'deleted'
    WHEN [PublicationDate] > GETDATE() THEN 'unavailable'
    WHEN [Quantity] > 0 THEN 'available'
    ELSE 'out-of-stock'
END", stored: false);
        // ref
        builder
            .HasMany(x => x.CartItems)
            .WithOne(x => x.Book)
            .HasForeignKey(x => x.BookId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.OrderItems)
            .WithOne(x => x.Book)
            .HasForeignKey(x => x.BookId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.Reviews)
            .WithOne(x => x.Book)
            .HasForeignKey(x => x.BookId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.WishlistItems)
            .WithOne(x => x.Book)
            .HasForeignKey(x => x.BookId)
            .OnDelete(DeleteBehavior.Restrict);
        // global queries
        builder.HasQueryFilter(x => x.Status == "available");
    }
}
