﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class ReviewConfiguration : IEntityTypeConfiguration<Review>
{
    public void Configure(EntityTypeBuilder<Review> builder)
    {
        builder.ToTable("Review", x => x.HasCheckConstraint("CK_Review_Rating", "[Rating] > 0 AND [Rating] < 11"));
        builder
            .HasKey(x => new { x.UserId, x.OrderId, x.BookId })
            .HasName("PK_Review_UserId_BookId_OrderItemId");
        builder
            .Property(x => x.Rating)
            .HasDefaultValue(1);
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.User)
            .WithMany(x => x.Reviews)
            .HasForeignKey(x => x.UserId)
            .HasConstraintName("FK_Review_UserId");
        builder
            .HasOne(x => x.Book)
            .WithMany(x => x.Reviews)
            .HasForeignKey(x => x.BookId)
            .HasConstraintName("FK_Review_BookId");
        builder
            .HasOne(x => x.OrderItem)
            .WithOne(x => x.Review)
            .HasForeignKey<Review>(x => new { x.OrderId, x.BookId })
            .HasConstraintName("FK_Review_OrderItemId");
    }
}
