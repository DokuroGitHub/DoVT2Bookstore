﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class CartItemConfiguration : IEntityTypeConfiguration<CartItem>
{
    public void Configure(EntityTypeBuilder<CartItem> builder)
    {
        builder.ToTable("CartItem", x => x.HasCheckConstraint("CK_CartItem_Quantity", "[Quantity] > 0"));
        builder
            .HasKey(x => new { x.CartId, x.BookId })
            .HasName("PK_CartItem_CartId_BookId");
        builder
            .Property(x => x.Quantity)
            .HasDefaultValue(1);
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.Cart)
            .WithMany(x => x.CartItems)
            .HasForeignKey(x => x.CartId)
            .HasConstraintName("FK_CartItem_CartId");
        builder
            .HasOne(x => x.Book)
            .WithMany(x => x.CartItems)
            .HasForeignKey(x => x.BookId)
            .HasConstraintName("FK_CartItem_BookId");
    }
}
