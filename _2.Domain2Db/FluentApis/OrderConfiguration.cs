﻿using _1.Domain.Entities;
using _1.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class OrderConfiguration : IEntityTypeConfiguration<Order>
{
    public void Configure(EntityTypeBuilder<Order> builder)
    {
        builder.ToTable("Order", x =>
        {
            x.HasCheckConstraint("CK_Order_OriginalTotalPrice", "[OriginalTotalPrice] >= 0");
            x.HasCheckConstraint("CK_Order_TotalDiscount", "[TotalDiscount] >= 0");
            x.HasCheckConstraint("CK_Order_TotalPrice", "[TotalPrice] >= 0");
            x.HasCheckConstraint("CK_Order_Prices", "[TotalPrice] >= [OriginalTotalPrice] - [TotalDiscount]");
            x.HasCheckConstraint("CK_Order_ItemCount", "[ItemCount] > 0");
            x.HasCheckConstraint("CK_Order_Status", "[Status] = 'Cart' OR [Status] = 'Paid' OR [Status] = 'Delivered' OR [Status] = 'Canceled'");
        });
        builder
            .HasKey(x => x.Id)
            .HasName("PK_Order_Id");
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        builder
            .Property(x => x.Currency)
            .HasDefaultValue("VND");
        builder
            .Property(x => x.Status)
            .HasDefaultValue("Cart");
        // builder.Property(x => x.Status)
        //     .HasConversion(
        //         v => v.ToString(),
        //         v => (OrderStatus)Enum.Parse(typeof(OrderStatus), v.ToString())
        //     );
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        builder
            .Property(x => x.UpdatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.User)
            .WithMany(x => x.Orders)
            .HasForeignKey(x => x.UserId)
            .HasConstraintName("FK_Order_UserId");
        builder
            .HasMany(x => x.OrderItems)
            .WithOne(x => x.Order)
            .HasForeignKey(x => x.OrderId)
            .OnDelete(DeleteBehavior.Restrict);
        builder
            .HasMany(x => x.OrderEvents)
            .WithOne(x => x.Order)
            .HasForeignKey(x => x.OrderId)
            .OnDelete(DeleteBehavior.Restrict);
    }
}
