﻿using _1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _2.Domain2Db.FluentApis;

public class OrderEventConfiguration : IEntityTypeConfiguration<OrderEvent>
{
    public void Configure(EntityTypeBuilder<OrderEvent> builder)
    {
        builder.ToTable("OrderEvent", x => x.HasCheckConstraint("CK_OrderEvent_Event", "[Event] = 'OrderUpdated' OR [Event] = 'OrderDeleted' OR [Event] = 'OrderPaid' OR [Event] = 'OrderCanceled' OR [Event] = 'OrderReturned' OR [Event] = 'OrderRefunded' OR [Event] = 'OrderShipped' OR [Event] = 'OrderDelivered' OR [Event] = 'OrderCompleted' OR [Event] = 'OrderFailed' OR [Event] = 'OrderExpired'"));
        builder
            .HasKey(x => x.Id)
            .HasName("PK_OrderEvent_Id");
        builder
            .Property(x => x.Id)
            .ValueGeneratedOnAdd();
        builder
            .Property(x => x.Event)
            .HasDefaultValue("OrderUpdated");
        builder
            .Property(x => x.CreatedAt)
            .HasDefaultValueSql("GETDATE()");
        // ref
        builder
            .HasOne(x => x.Order)
            .WithMany(x => x.OrderEvents)
            .HasForeignKey(x => x.OrderId)
            .HasConstraintName("FK_OrderEvent_OrderId");
    }
}
