using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace _2.Domain2Db;

public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
{
    ///<summary> to config to generate migrations </summary>
    public ApplicationDbContext CreateDbContext(string[] args)
    {
        // get connection string
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        var appsettings = new Appsettings();
        configuration.Bind(appsettings);

        // var connectionString = configuration.GetConnectionString("DokuroSQLConnectionString");
        var connectionString = appsettings.ConnectionStrings.DokuroSQLConnectionString;
        Console.WriteLine(connectionString);

        var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
        optionsBuilder.UseSqlServer(connectionString);

        return new ApplicationDbContext(optionsBuilder.Options);
    }
}
