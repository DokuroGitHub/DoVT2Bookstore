# Saved Ids

```json
User :{
"UserId": "66de0a9f-149f-4119-b080-6e667c8140dc",
"UserId": "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b",
"UserId": "7fd3cc4c-8ff6-499e-a195-24ced2739622",
"UserId": "510c6c9a-52af-4111-9d09-ac8a448a82fb",
"UserId": "29c81547-fd29-4f94-b60c-a7c5f2929b54",
}

Book: {
"BookId": "24386350-98ad-49f3-aa0e-a714bdcbf0bd",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"BookId": "df2328f2-c863-428c-b933-2828d479eae7",
"BookId": "1e079dbf-206a-4235-971a-101e7fdbbdd1",
"BookId": "f1e3f56a-24ad-40e0-a02d-fc5bfb5bbbaa",
}

Cart :{
"CartId": "ea87d2bc-8ba4-4b81-b007-bf9321b0240a",
"CartId": "beda2e7f-4af9-4267-ad86-af752adccaca",
"CartId": "9c4ffb1b-0544-4a04-8863-fc56fe412354",
"CartId": "d671c9f4-9175-46f4-9345-32b9d01c7448",
"CartId": "c3279923-3b93-49fd-930c-af6f9e7e6b9a",
}

CartItem:{
"CartId": "ea87d2bc-8ba4-4b81-b007-bf9321b0240a",
"BookId": "24386350-98ad-49f3-aa0e-a714bdcbf0bd",
"Quantity": 1,
//
"CartId": "beda2e7f-4af9-4267-ad86-af752adccaca",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"Quantity": 2,
//
"CartId": "9c4ffb1b-0544-4a04-8863-fc56fe412354",
"BookId": "df2328f2-c863-428c-b933-2828d479eae7",
"Quantity": 3,
//
"CartId": "d671c9f4-9175-46f4-9345-32b9d01c7448",
"BookId": "1e079dbf-206a-4235-971a-101e7fdbbdd1",
"Quantity": 4,
//
"CartId": "d671c9f4-9175-46f4-9345-32b9d01c7448",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"Quantity": 5,
}

Order: {
"OrderId": "0ba5140d-be29-4661-b7f5-0df4c911747a",
"OrderId": "990f53e5-acd4-4148-a765-416021538766",
}

OrderEvent: {
"OrderEventId": "3701f324-695a-4118-8502-698479566194",
"OrderEventId": "876d6d3d-933a-4bf6-8902-7427ddd6ef70",
"OrderEventId": "d3365344-54d5-4ab1-a3ce-f307ceb1b6c6",
"OrderEventId": "93595f54-c89d-4ae8-84f2-44534d28a1b4",
"OrderEventId": "13032816-dd4e-4ea3-b863-48da8c20d478",
}

OrderItem: {
"OrderId": "0ba5140d-be29-4661-b7f5-0df4c911747a",
"BookId": "24386350-98ad-49f3-aa0e-a714bdcbf0bd",
"Quantity": 1,
//
"OrderId": "0ba5140d-be29-4661-b7f5-0df4c911747a",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"Quantity": 2,
//
"OrderId": "0ba5140d-be29-4661-b7f5-0df4c911747a",
"BookId": "df2328f2-c863-428c-b933-2828d479eae7",
"Quantity": 3,
//
"OrderId": "990f53e5-acd4-4148-a765-416021538766",
"BookId": "1e079dbf-206a-4235-971a-101e7fdbbdd1",
"Quantity": 4,
//
"OrderId": "990f53e5-acd4-4148-a765-416021538766",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"Quantity": 5,
}

Review: {
"UserId": "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b",
"OrderId": "990f53e5-acd4-4148-a765-416021538766",
"BookId": "1e079dbf-206a-4235-971a-101e7fdbbdd1",
"Rating": 5
}

Wishlist: {
"WishlistId": "0f634ddb-fd58-4946-86a6-5b1800dbff0e",
"WishlistId": "cdae0772-d3d7-4c5b-9719-1e4b6ecd27be",
"WishlistId": "0e2b0eb3-0759-4e94-9a53-c7c432aa6a90",
"WishlistId": "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b",
"WishlistId": "a6062189-2a48-40dc-b73e-5f8b09e9549b",
}

WishlistItem: {
//
"WishlistId": "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b",
"BookId": "24386350-98ad-49f3-aa0e-a714bdcbf0bd",
"Quantity": 1,
//
"WishlistId": "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"Quantity": 2,
//
"WishlistId": "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b",
"BookId": "1e079dbf-206a-4235-971a-101e7fdbbdd1",
"Quantity": 3,
//
"WishlistId": "a6062189-2a48-40dc-b73e-5f8b09e9549b",
"BookId": "df2328f2-c863-428c-b933-2828d479eae7",
"Quantity": 4,
//
"WishlistId": "a6062189-2a48-40dc-b73e-5f8b09e9549b",
"BookId": "63c0093e-87fd-448a-8da3-31885eb1ab6f",
"Quantity": 5,
}

```
