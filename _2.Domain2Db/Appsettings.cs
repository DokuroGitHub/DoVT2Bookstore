namespace _2.Domain2Db;

#pragma warning disable
public class Appsettings
{
    public ConnectionStrings ConnectionStrings { get; set; }
}

public class ConnectionStrings
{
    public string DokuroSQLConnectionString { get; set; }
}
