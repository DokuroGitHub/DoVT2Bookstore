﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace _2.Domain2Db.Migrations
{
    /// <inheritdoc />
    public partial class update1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "29c81547-fd29-4f94-b60c-a7c5f2929b54",
                column: "Email",
                value: "gidoquenroi2@gmail.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "510c6c9a-52af-4111-9d09-ac8a448a82fb",
                column: "Email",
                value: "gidoquenroi1@gmail.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b",
                column: "Email",
                value: "dokuro.jp@gmail.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "66de0a9f-149f-4119-b080-6e667c8140dc",
                column: "Email",
                value: "tamthoidetrong@gmail.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "7fd3cc4c-8ff6-499e-a195-24ced2739622",
                column: "Email",
                value: "dovt58@gmail.com");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "29c81547-fd29-4f94-b60c-a7c5f2929b54",
                column: "Email",
                value: "breace4@adobe.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "510c6c9a-52af-4111-9d09-ac8a448a82fb",
                column: "Email",
                value: "kwhitlow3@sphinn.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b",
                column: "Email",
                value: "twemes1@github.com");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "66de0a9f-149f-4119-b080-6e667c8140dc",
                column: "Email",
                value: "gfey0@usgs.gov");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "7fd3cc4c-8ff6-499e-a195-24ced2739622",
                column: "Email",
                value: "sstothart2@t.co");
        }
    }
}
