﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace _2.Domain2Db.Migrations
{
    /// <inheritdoc />
    public partial class InitDB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Author = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CoverImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: false, defaultValue: "VND"),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    PublicationDate = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    AvgRating = table.Column<float>(type: "real", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false, computedColumnSql: "\r\nCASE\r\n    WHEN [DeletedAt] IS NOT NULL THEN 'deleted'\r\n    WHEN [PublicationDate] > GETDATE() THEN 'unavailable'\r\n    WHEN [Quantity] > 0 THEN 'available'\r\n    ELSE 'out-of-stock'\r\nEND", stored: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book_Id", x => x.Id);
                    table.CheckConstraint("CK_Book_AvgRating", "[AvgRating] >= 0 AND [AvgRating] <= 5");
                    table.CheckConstraint("CK_Book_Price", "[Price] >= 0");
                    table.CheckConstraint("CK_Book_Quantity", "[Quantity] >= 0");
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Avatar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    IsVerified = table.Column<bool>(type: "bit", nullable: false),
                    DisplayName = table.Column<string>(type: "nvarchar(max)", nullable: false, computedColumnSql: "\r\nCASE\r\n    WHEN [FirstName] IS NOT NULL AND [FirstName] <> ''\r\n        THEN CASE\r\n  			WHEN [LastName] IS NOT NULL AND [LastName] <> '' \r\n       			THEN [FirstName] + ' ' + [LastName]\r\n       		ELSE [FirstName]\r\n  		END\r\n    WHEN [LastName] IS NOT NULL AND [LastName] <> '' THEN [LastName]\r\n    WHEN [Email] IS NOT NULL AND [Email] <> '' THEN [Email]\r\n	ELSE [Id]\r\nEND", stored: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Id", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cart",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    OriginalTotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalDiscount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: false, defaultValue: "VND"),
                    ItemCount = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cart_Id", x => x.Id);
                    table.CheckConstraint("CK_Cart_ItemCount", "[ItemCount] >= 0");
                    table.CheckConstraint("CK_Cart_OriginalTotalPrice", "[OriginalTotalPrice] >= 0");
                    table.CheckConstraint("CK_Cart_Prices", "[TotalPrice] >= [OriginalTotalPrice] - [TotalDiscount]");
                    table.CheckConstraint("CK_Cart_TotalDiscount", "[TotalDiscount] >= 0");
                    table.CheckConstraint("CK_Cart_TotalPrice", "[TotalPrice] >= 0");
                    table.ForeignKey(
                        name: "FK_Cart_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    OriginalTotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalDiscount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Currency = table.Column<string>(type: "nvarchar(max)", nullable: false, defaultValue: "VND"),
                    ItemCount = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: false, defaultValue: "Cart"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order_Id", x => x.Id);
                    table.CheckConstraint("CK_Order_ItemCount", "[ItemCount] > 0");
                    table.CheckConstraint("CK_Order_OriginalTotalPrice", "[OriginalTotalPrice] >= 0");
                    table.CheckConstraint("CK_Order_Prices", "[TotalPrice] >= [OriginalTotalPrice] - [TotalDiscount]");
                    table.CheckConstraint("CK_Order_Status", "[Status] = 'Cart' OR [Status] = 'Paid' OR [Status] = 'Delivered' OR [Status] = 'Canceled'");
                    table.CheckConstraint("CK_Order_TotalDiscount", "[TotalDiscount] >= 0");
                    table.CheckConstraint("CK_Order_TotalPrice", "[TotalPrice] >= 0");
                    table.ForeignKey(
                        name: "FK_Order_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserCredential",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    HashPassword = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Role = table.Column<string>(type: "nvarchar(max)", nullable: false, defaultValue: "User"),
                    CreditBalance = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCredential_UserId", x => x.UserId);
                    table.CheckConstraint("CK_UserCredential_CreditBalance", "[CreditBalance] >= 0");
                    table.CheckConstraint("CK_UserCredential_Role", "[Role] = 'Admin' or [Role] = 'User' or [Role] = 'Guest'");
                    table.ForeignKey(
                        name: "FK_UserCredential_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Wishlist",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemCount = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wishlist_Id", x => x.Id);
                    table.CheckConstraint("CK_Wishlist_ItemCount", "[ItemCount] >= 0");
                    table.ForeignKey(
                        name: "FK_Wishlist_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CartItem",
                columns: table => new
                {
                    CartId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BookId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CartItem_CartId_BookId", x => new { x.CartId, x.BookId });
                    table.CheckConstraint("CK_CartItem_Quantity", "[Quantity] > 0");
                    table.ForeignKey(
                        name: "FK_CartItem_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CartItem_CartId",
                        column: x => x.CartId,
                        principalTable: "Cart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderEvent",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    OrderId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Event = table.Column<string>(type: "nvarchar(max)", nullable: false, defaultValue: "OrderUpdated"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderEvent_Id", x => x.Id);
                    table.CheckConstraint("CK_OrderEvent_Event", "[Event] = 'OrderUpdated' OR [Event] = 'OrderDeleted' OR [Event] = 'OrderPaid' OR [Event] = 'OrderCanceled' OR [Event] = 'OrderReturned' OR [Event] = 'OrderRefunded' OR [Event] = 'OrderShipped' OR [Event] = 'OrderDelivered' OR [Event] = 'OrderCompleted' OR [Event] = 'OrderFailed' OR [Event] = 'OrderExpired'");
                    table.ForeignKey(
                        name: "FK_OrderEvent_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderItem",
                columns: table => new
                {
                    OrderId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BookId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItem_OrderId_BookId", x => new { x.OrderId, x.BookId });
                    table.CheckConstraint("CK_OrderItem_Quantity", "[Quantity] > 0");
                    table.ForeignKey(
                        name: "FK_OrderItem_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderItem_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WishlistItem",
                columns: table => new
                {
                    WishlistId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BookId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WishlistItem_WishlistId_BookId", x => new { x.WishlistId, x.BookId });
                    table.CheckConstraint("CK_WishlistItem_Quantity", "[Quantity] > 0");
                    table.ForeignKey(
                        name: "FK_WishlistItem_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WishlistItem_WishlistId",
                        column: x => x.WishlistId,
                        principalTable: "Wishlist",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Review",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    OrderId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BookId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rating = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false, defaultValueSql: "GETDATE()"),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true, defaultValueSql: "GETDATE()"),
                    UpdatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Review_UserId_BookId_OrderItemId", x => new { x.UserId, x.OrderId, x.BookId });
                    table.CheckConstraint("CK_Review_Rating", "[Rating] > 0 AND [Rating] < 11");
                    table.ForeignKey(
                        name: "FK_Review_BookId",
                        column: x => x.BookId,
                        principalTable: "Book",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Review_OrderItemId",
                        columns: x => new { x.OrderId, x.BookId },
                        principalTable: "OrderItem",
                        principalColumns: new[] { "OrderId", "BookId" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Review_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "Id", "Author", "AvgRating", "CoverImage", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "Genre", "Price", "PublicationDate", "Quantity", "Title", "UpdatedBy" },
                values: new object[] { "1e079dbf-206a-4235-971a-101e7fdbbdd1", "Gilbertina Oughtright", 5f, "http://dummyimage.com/137x100.png/ff4444/ffffff", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar", "Crime|Drama", 78m, new DateTime(2023, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, "innovate frictionless solutions", null });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "Id", "Author", "AvgRating", "CoverImage", "CreatedBy", "Currency", "DeletedAt", "DeletedBy", "Description", "Genre", "Price", "PublicationDate", "Quantity", "Title", "UpdatedBy" },
                values: new object[,]
                {
                    { "24386350-98ad-49f3-aa0e-a714bdcbf0bd", "Gilly Kiffe", null, "http://dummyimage.com/152x100.png/5fa2dd/ffffff", "66de0a9f-149f-4119-b080-6e667c8140dc", "PHP", null, null, "elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit", "Horror|Thriller", 64m, new DateTime(2023, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "disintermediate dot-com paradigms", null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "Laurel Tambling", null, "http://dummyimage.com/161x100.png/dddddd/000000", "66de0a9f-149f-4119-b080-6e667c8140dc", "EUR", null, null, "sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris", "Comedy", 85m, new DateTime(2023, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "synthesize strategic web services", null }
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "Id", "Author", "AvgRating", "CoverImage", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "Genre", "Price", "PublicationDate", "Quantity", "Title", "UpdatedBy" },
                values: new object[,]
                {
                    { "df2328f2-c863-428c-b933-2828d479eae7", "Mar Panswick", null, "http://dummyimage.com/134x100.png/5fa2dd/ffffff", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend", "Drama", 14m, new DateTime(2023, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "transform sticky systems", null },
                    { "f1e3f56a-24ad-40e0-a02d-fc5bfb5bbbaa", "Hamilton Greatland", null, "http://dummyimage.com/180x100.png/5fa2dd/ffffff", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec", "Drama|Romance", 20m, new DateTime(2023, 4, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, "expedite cross-platform systems", null }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Avatar", "CreatedBy", "DeletedAt", "DeletedBy", "Email", "FirstName", "IsVerified", "LastName", "UpdatedBy" },
                values: new object[,]
                {
                    { "29c81547-fd29-4f94-b60c-a7c5f2929b54", "https://i.redd.it/ou5v1znp08z91.jpg", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "breace4@adobe.com", "Mills Inc", true, "Oyoyo", null },
                    { "510c6c9a-52af-4111-9d09-ac8a448a82fb", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4H5NgErIK-Rn7ir6y_mnhXKZ5e1vSA8X2vg&usqp=CAU", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "kwhitlow3@sphinn.com", "Gulgowski Inc", true, "Realcube", null },
                    { "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b", "https://w0.peakpx.com/wallpaper/368/441/HD-wallpaper-cute-anime-girl-anime-cat-girl-anime-girl-cartoon-cat-girl-cute-anime-thumbnail.jpg", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "twemes1@github.com", "Gerlach, Pollich and Cormier", true, "Realpoint", null },
                    { "66de0a9f-149f-4119-b080-6e667c8140dc", "https://www.animesenpai.net/wp-content/uploads/2022/09/maxresdefault-4-1024x576.jpg", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "gfey0@usgs.gov", "Becker and Sons", true, "Realcube", null },
                    { "7fd3cc4c-8ff6-499e-a195-24ced2739622", "https://www.animeinformer.com/wp-content/uploads/2022/09/kawaii-anime-girl-cool-608x1024.png", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "sstothart2@t.co", "Bailey LLC", true, "Eare", null }
                });

            migrationBuilder.InsertData(
                table: "Cart",
                columns: new[] { "Id", "CreatedBy", "Currency", "DeletedAt", "DeletedBy", "ItemCount", "OriginalTotalPrice", "TotalDiscount", "TotalPrice", "UpdatedBy", "UserId" },
                values: new object[,]
                {
                    { "9c4ffb1b-0544-4a04-8863-fc56fe412354", "66de0a9f-149f-4119-b080-6e667c8140dc", "USD", null, null, 3, 687m, 99m, 600m, null, "7fd3cc4c-8ff6-499e-a195-24ced2739622" },
                    { "beda2e7f-4af9-4267-ad86-af752adccaca", "66de0a9f-149f-4119-b080-6e667c8140dc", "EUR", null, null, 2, 322m, 25m, 300m, null, "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b" },
                    { "c3279923-3b93-49fd-930c-af6f9e7e6b9a", "66de0a9f-149f-4119-b080-6e667c8140dc", "ARS", null, null, 0, 0m, 0m, 0m, null, "29c81547-fd29-4f94-b60c-a7c5f2929b54" },
                    { "d671c9f4-9175-46f4-9345-32b9d01c7448", "66de0a9f-149f-4119-b080-6e667c8140dc", "CNY", null, null, 9, 246m, 26m, 250m, null, "510c6c9a-52af-4111-9d09-ac8a448a82fb" },
                    { "ea87d2bc-8ba4-4b81-b007-bf9321b0240a", "66de0a9f-149f-4119-b080-6e667c8140dc", "ARS", null, null, 1, 96m, 9m, 100m, null, "66de0a9f-149f-4119-b080-6e667c8140dc" }
                });

            migrationBuilder.InsertData(
                table: "Order",
                columns: new[] { "Id", "CreatedBy", "Currency", "DeletedAt", "DeletedBy", "ItemCount", "OriginalTotalPrice", "Status", "TotalDiscount", "TotalPrice", "UpdatedBy", "UserId" },
                values: new object[,]
                {
                    { "0ba5140d-be29-4661-b7f5-0df4c911747a", "66de0a9f-149f-4119-b080-6e667c8140dc", "VND", null, null, 6, 344m, "Cart", 33m, 384m, null, "66de0a9f-149f-4119-b080-6e667c8140dc" },
                    { "990f53e5-acd4-4148-a765-416021538766", "66de0a9f-149f-4119-b080-6e667c8140dc", "USD", null, null, 9, 997m, "Delivered", 66m, 999m, null, "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b" }
                });

            migrationBuilder.InsertData(
                table: "UserCredential",
                columns: new[] { "UserId", "CreatedAt", "CreatedBy", "CreditBalance", "DeletedAt", "DeletedBy", "HashPassword", "UpdatedBy", "Username" },
                values: new object[,]
                {
                    { "29c81547-fd29-4f94-b60c-a7c5f2929b54", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "66de0a9f-149f-4119-b080-6e667c8140dc", 99m, null, null, "$2a$11$2PJMSufmjtIbktnDZ8nbHejByAc9I.wkVQx9u.uzlyye8NhEPMNl6", null, "breace4@adobe.com" },
                    { "510c6c9a-52af-4111-9d09-ac8a448a82fb", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "66de0a9f-149f-4119-b080-6e667c8140dc", 99m, null, null, "$2a$11$2PJMSufmjtIbktnDZ8nbHejByAc9I.wkVQx9u.uzlyye8NhEPMNl6", null, "kwhitlow3@sphinn.com" }
                });

            migrationBuilder.InsertData(
                table: "UserCredential",
                columns: new[] { "UserId", "CreatedAt", "CreatedBy", "CreditBalance", "DeletedAt", "DeletedBy", "HashPassword", "Role", "UpdatedBy", "Username" },
                values: new object[,]
                {
                    { "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "66de0a9f-149f-4119-b080-6e667c8140dc", 1000m, null, null, "$2a$11$2PJMSufmjtIbktnDZ8nbHejByAc9I.wkVQx9u.uzlyye8NhEPMNl6", "User", null, "twemes1@github.com" },
                    { "66de0a9f-149f-4119-b080-6e667c8140dc", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "66de0a9f-149f-4119-b080-6e667c8140dc", 0m, null, null, "$2a$11$2PJMSufmjtIbktnDZ8nbHejByAc9I.wkVQx9u.uzlyye8NhEPMNl6", "Admin", null, "gfey0@usgs.gov" }
                });

            migrationBuilder.InsertData(
                table: "UserCredential",
                columns: new[] { "UserId", "CreatedAt", "CreatedBy", "CreditBalance", "DeletedAt", "DeletedBy", "HashPassword", "UpdatedBy", "Username" },
                values: new object[] { "7fd3cc4c-8ff6-499e-a195-24ced2739622", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "66de0a9f-149f-4119-b080-6e667c8140dc", 0m, null, null, "$2a$11$2PJMSufmjtIbktnDZ8nbHejByAc9I.wkVQx9u.uzlyye8NhEPMNl6", null, "sstothart2@t.co" });

            migrationBuilder.InsertData(
                table: "Wishlist",
                columns: new[] { "Id", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "ItemCount", "UpdatedBy", "UserId" },
                values: new object[,]
                {
                    { "0e2b0eb3-0759-4e94-9a53-c7c432aa6a90", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum", 0, null, "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b" },
                    { "0f634ddb-fd58-4946-86a6-5b1800dbff0e", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus", 0, null, "66de0a9f-149f-4119-b080-6e667c8140dc" },
                    { "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "tellus nisi eu orci mauris lacinia sapien quis libero nullam", 6, null, "7fd3cc4c-8ff6-499e-a195-24ced2739622" },
                    { "a6062189-2a48-40dc-b73e-5f8b09e9549b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum", 9, null, "510c6c9a-52af-4111-9d09-ac8a448a82fb" },
                    { "cdae0772-d3d7-4c5b-9719-1e4b6ecd27be", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo", 0, null, "60ec16d2-a05a-4b35-a573-ec8a2d1bf67b" }
                });

            migrationBuilder.InsertData(
                table: "CartItem",
                columns: new[] { "BookId", "CartId", "CreatedBy", "DeletedAt", "DeletedBy", "Quantity", "UpdatedBy" },
                values: new object[,]
                {
                    { "df2328f2-c863-428c-b933-2828d479eae7", "9c4ffb1b-0544-4a04-8863-fc56fe412354", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 3, null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "beda2e7f-4af9-4267-ad86-af752adccaca", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 2, null },
                    { "1e079dbf-206a-4235-971a-101e7fdbbdd1", "d671c9f4-9175-46f4-9345-32b9d01c7448", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 4, null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "d671c9f4-9175-46f4-9345-32b9d01c7448", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 5, null },
                    { "24386350-98ad-49f3-aa0e-a714bdcbf0bd", "ea87d2bc-8ba4-4b81-b007-bf9321b0240a", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 1, null }
                });

            migrationBuilder.InsertData(
                table: "OrderEvent",
                columns: new[] { "Id", "CreatedBy", "DeletedAt", "DeletedBy", "Description", "Event", "OrderId", "UpdatedAt", "UpdatedBy" },
                values: new object[,]
                {
                    { "13032816-dd4e-4ea3-b863-48da8c20d478", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui", "OrderCanceled", "990f53e5-acd4-4148-a765-416021538766", null, null },
                    { "3701f324-695a-4118-8502-698479566194", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum", "OrderUpdated", "0ba5140d-be29-4661-b7f5-0df4c911747a", null, null },
                    { "876d6d3d-933a-4bf6-8902-7427ddd6ef70", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices", "OrderDelivered", "0ba5140d-be29-4661-b7f5-0df4c911747a", null, null },
                    { "93595f54-c89d-4ae8-84f2-44534d28a1b4", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper", "OrderUpdated", "990f53e5-acd4-4148-a765-416021538766", null, null },
                    { "d3365344-54d5-4ab1-a3ce-f307ceb1b6c6", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, "vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet", "OrderCompleted", "0ba5140d-be29-4661-b7f5-0df4c911747a", null, null }
                });

            migrationBuilder.InsertData(
                table: "OrderItem",
                columns: new[] { "BookId", "OrderId", "CreatedBy", "DeletedAt", "DeletedBy", "Quantity", "UpdatedBy" },
                values: new object[,]
                {
                    { "24386350-98ad-49f3-aa0e-a714bdcbf0bd", "0ba5140d-be29-4661-b7f5-0df4c911747a", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 1, null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "0ba5140d-be29-4661-b7f5-0df4c911747a", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 2, null },
                    { "df2328f2-c863-428c-b933-2828d479eae7", "0ba5140d-be29-4661-b7f5-0df4c911747a", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 3, null },
                    { "1e079dbf-206a-4235-971a-101e7fdbbdd1", "990f53e5-acd4-4148-a765-416021538766", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 4, null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "990f53e5-acd4-4148-a765-416021538766", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 5, null }
                });

            migrationBuilder.InsertData(
                table: "WishlistItem",
                columns: new[] { "BookId", "WishlistId", "CreatedBy", "DeletedAt", "DeletedBy", "Quantity", "UpdatedBy" },
                values: new object[,]
                {
                    { "1e079dbf-206a-4235-971a-101e7fdbbdd1", "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 3, null },
                    { "24386350-98ad-49f3-aa0e-a714bdcbf0bd", "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 1, null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "6a5cd0c9-187b-4d83-9d05-f5e8900ab93b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 2, null },
                    { "63c0093e-87fd-448a-8da3-31885eb1ab6f", "a6062189-2a48-40dc-b73e-5f8b09e9549b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 5, null },
                    { "df2328f2-c863-428c-b933-2828d479eae7", "a6062189-2a48-40dc-b73e-5f8b09e9549b", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 4, null }
                });

            migrationBuilder.InsertData(
                table: "Review",
                columns: new[] { "BookId", "OrderId", "UserId", "Comment", "CreatedBy", "DeletedAt", "DeletedBy", "Rating", "UpdatedBy" },
                values: new object[] { "24386350-98ad-49f3-aa0e-a714bdcbf0bd", "0ba5140d-be29-4661-b7f5-0df4c911747a", "66de0a9f-149f-4119-b080-6e667c8140dc", "congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat", "66de0a9f-149f-4119-b080-6e667c8140dc", null, null, 5, null });

            migrationBuilder.CreateIndex(
                name: "IX_Cart_UserId",
                table: "Cart",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CartItem_BookId",
                table: "CartItem",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_UserId",
                table: "Order",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderEvent_OrderId",
                table: "OrderEvent",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_BookId",
                table: "OrderItem",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Review_BookId",
                table: "Review",
                column: "BookId");

            migrationBuilder.CreateIndex(
                name: "IX_Review_OrderId_BookId",
                table: "Review",
                columns: new[] { "OrderId", "BookId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_Email",
                table: "User",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_UserCredential_Username",
                table: "UserCredential",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Wishlist_UserId",
                table: "Wishlist",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_WishlistItem_BookId",
                table: "WishlistItem",
                column: "BookId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CartItem");

            migrationBuilder.DropTable(
                name: "OrderEvent");

            migrationBuilder.DropTable(
                name: "Review");

            migrationBuilder.DropTable(
                name: "UserCredential");

            migrationBuilder.DropTable(
                name: "WishlistItem");

            migrationBuilder.DropTable(
                name: "Cart");

            migrationBuilder.DropTable(
                name: "OrderItem");

            migrationBuilder.DropTable(
                name: "Wishlist");

            migrationBuilder.DropTable(
                name: "Book");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
