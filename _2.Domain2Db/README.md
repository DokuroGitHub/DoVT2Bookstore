# _2.Domain2Db

```bash
dotnet new classlib

dotnet add reference "../_1.Domain/_1.Domain.csproj"

#
dotnet build
dotnet ef migrations add InitDB
dotnet ef database update
#
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.Extensions.Configuration.Json
dotnet add package Microsoft.Extensions.Configuration.Binder
dotnet add package Newtonsoft.Json
#

```
