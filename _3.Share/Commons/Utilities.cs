using System.Dynamic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace _3.Share.Commons;

public class Utilities
{
    /// <summary>"id,title,description"</summary>
    public static string GetProps<T>(Expression<Func<T, object>> parameters)
    {
        var requestedParametersString = new StringBuilder();
        if (parameters != null && parameters.Body != null)
        {
            if (parameters.Body is NewExpression body && body.Members != null && body.Members.Any())
            {
                foreach (var member in body.Members)
                {
                    requestedParametersString.Append(member.Name + ",");
                }
            }
        }
        return requestedParametersString.ToString();
    }

    public static object CreateAnonymousObject<T>(T obj, string selectedFields)
    {
        var selectedProperties = selectedFields.Split(',').Select(f => typeof(T).GetProperty(f.Trim()));
        var anonymousObject = new ExpandoObject() as IDictionary<string, object>;
        foreach (var prop in selectedProperties)
        {
            anonymousObject.Add(prop.Name, prop.GetValue(obj));
        }

        return anonymousObject;
    }

    public static Expression<Func<TEntity, TEntity>> ConvertExpression<TEntity>(Expression<Func<TEntity, object>> selector)
    {
        var parameter = selector.Parameters[0];
        var body = Expression.Convert(selector.Body, typeof(TEntity));
        return Expression.Lambda<Func<TEntity, TEntity>>(body, parameter);
    }

    public static Expression<Func<TEntity, TEntity>> CreateSelectExpression<TEntity>(
        params Expression<Func<TEntity, object>>[] propertySelectors)
    {
        var parameter = Expression.Parameter(typeof(TEntity), "x");
        var memberBindings = new List<MemberBinding>();

        foreach (var selector in propertySelectors)
        {
            var memberExpression = selector.Body as MemberExpression;

            if (memberExpression == null)
            {
                throw new ArgumentException("Property selector must be a member expression");
            }

            var propertyInfo = memberExpression.Member as PropertyInfo;

            if (propertyInfo == null)
            {
                throw new ArgumentException("Property selector must be a property expression");
            }

            var propertyExpression = Expression.Property(parameter, propertyInfo);
            var memberBinding = Expression.Bind(propertyInfo, propertyExpression);

            memberBindings.Add(memberBinding);
        }

        var newExpression = Expression.New(typeof(TEntity));
        var memberInitExpression = Expression.MemberInit(newExpression, memberBindings);

        return Expression.Lambda<Func<TEntity, TEntity>>(memberInitExpression, parameter);
    }

}
