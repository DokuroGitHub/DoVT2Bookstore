﻿using System.Linq.Expressions;

namespace _3.Share.Commons;

public static class ExpressionExtensions
{
    public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> expr)
    {
        return Expression.Lambda<Func<T, bool>>(
            Expression.Not(expr.Body), expr.Parameters[0]);
    }

    public static Expression<Func<T, bool>> AndAlso<T>(
       this Expression<Func<T, bool>> expr1,
       Expression<Func<T, bool>> expr2)
    {
        var parameter = Expression.Parameter(typeof(T));

        var leftVisitor = new ReplaceExpressionVisitor(expr1.Parameters[0], parameter);
        var left = leftVisitor.Visit(expr1.Body);
        if (left == null)
            throw new InvalidOperationException("left is null");

        var rightVisitor = new ReplaceExpressionVisitor(expr2.Parameters[0], parameter);
        var right = rightVisitor.Visit(expr2.Body);
        if (right == null)
            throw new InvalidOperationException("right is null");

        return Expression.Lambda<Func<T, bool>>(
            Expression.AndAlso(left, right), parameter);
    }

    public static Expression<Func<TInput, object>> ToObjectExpression<TInput, TOutput>(this Expression<Func<TInput, TOutput>> expression)
    {
        Expression converted = Expression.Convert(expression.Body, typeof(object));
        return Expression.Lambda<Func<TInput, object>>(converted, expression.Parameters);
    }

    private class ReplaceExpressionVisitor : ExpressionVisitor
    {
        private readonly Expression _oldValue;
        private readonly Expression _newValue;

        public ReplaceExpressionVisitor(Expression oldValue, Expression newValue)
        {
            _oldValue = oldValue;
            _newValue = newValue;
        }

        public override Expression? Visit(Expression? node)
        {
            if (node == _oldValue)
                return _newValue;
            return base.Visit(node);
        }
    }
}
