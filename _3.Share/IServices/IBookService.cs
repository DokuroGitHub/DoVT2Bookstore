using System.Linq.Expressions;
using _3.Share.ViewModels;
using _3.Share.ViewModels.Book;

namespace _3.Share.IServices;

public interface IBookService
{
    Task<PagedResult<BookDto>> GetPagedItems(
        string? search = null,
        string? includeProperties = null,
        string? selectFields = null,
        int pageIndex = 1,
        int pageSize = 10);
    Task<PagedResult<BookDto>> Search(string? search = null, int pageIndex = 1, int pageSize = 10);
    Task<PagedResult<BookDto>> AdvancedSearch(
        string? title = null,
        string? description = null,
        string? author = null,
        string? genre = null,
        string? status = null,
        string? includeProperties = null,
        int pageIndex = 1,
        int pageSize = 10);
    Task<BookDto?> GetOne(string id);
    Task<BookDto> Create(BookCreateDto dto);
    Task<BookDto?> Update(string id, BookUpdateDto dto);
    Task<ResponseTypeId> Delete(string id);
}
