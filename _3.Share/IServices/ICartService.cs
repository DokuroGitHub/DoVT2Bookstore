using _3.Share.ViewModels;
using _3.Share.ViewModels.Cart;

namespace _3.Share.IServices;

public interface ICartService
{
    Task<PagedResult<CartDto>> GetMyCarts(int pageIndex, int pageSize);
    Task<CartDto?> GetOne(string id);
    Task<CartDto> Create(CartCreateDto dto);
    Task<ResponseTypeId> Delete(string id);
    Task<CartDto> Update(string id, CartUpdateDto dto);
}
