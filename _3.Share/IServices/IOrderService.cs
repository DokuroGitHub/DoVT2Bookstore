using _3.Share.ViewModels;
using _3.Share.ViewModels.Order;

namespace _3.Share.IServices;

public interface IOrderService
{
    Task<PagedResult<OrderDto>> GetMyOrders(int pageIndex, int pageSize);
    Task<OrderDto?> GetOne(string id);
    Task<OrderDto> Create(OrderCreateDto dto);
    Task<ResponseTypeId> Delete(string id);
    Task<OrderDto> Update(string id, OrderUpdateDto dto);
}
