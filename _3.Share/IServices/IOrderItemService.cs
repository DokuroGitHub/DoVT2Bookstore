using _3.Share.ViewModels;
using _3.Share.ViewModels.OrderItem;

namespace _3.Share.IServices;

public interface IOrderItemService
{
    Task<PagedResult<OrderItemDto>> GetMyOrderItems(int pageIndex, int pageSize);
    Task<OrderItemDto?> GetOne(string cartId, string bookId);
    Task<OrderItemDto> Create(OrderItemCreateDto dto);
    Task<ResponseTypeOrderIdBookId> Delete(string cartId, string bookId);
    Task<OrderItemDto> Update(string cartId, string bookId, OrderItemUpdateDto dto);
}
