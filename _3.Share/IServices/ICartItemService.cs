using _3.Share.ViewModels;
using _3.Share.ViewModels.CartItem;

namespace _3.Share.IServices;

public interface ICartItemService
{
    Task<PagedResult<CartItemDto>> GetMyCartItems(int pageIndex, int pageSize);
    Task<CartItemDto?> GetOne(string cartId, string bookId);
    Task<CartItemDto> Create(CartItemCreateDto dto);
    Task<ResponseTypeCartIdBookId> Delete(string cartId, string bookId);
    Task<CartItemDto> Update(string cartId, string bookId, CartItemUpdateDto dto);
}
