﻿using _3.Share.ViewModels;
using _3.Share.ViewModels.User;

namespace _3.Share.IServices;

public interface IAuthService
{
    Task<bool> IsUniqueUserAsync(string username);
    Task<LoginResponseDto?> LoginAsync(LoginRequestDto loginRequestDTO);
    Task<UserFlatDto?> RegisterAsync(RegisterationRequestDto registerationRequestDTO);
}
