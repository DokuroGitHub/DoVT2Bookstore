namespace _3.Share.ViewModels.Review;

#pragma warning disable
public class ReviewUpdateDto
{
    public string UserId { get; set; }
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public string? Comment { get; set; }
    public int Rating { get; set; }
}
