namespace _3.Share.ViewModels.Review;

#pragma warning disable
public class ReviewFlatDto
{
    public string UserId { get; set; }
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public string? Comment { get; set; }
    public int Rating { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
}
