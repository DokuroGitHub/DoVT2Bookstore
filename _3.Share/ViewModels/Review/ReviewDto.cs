using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.OrderItem;
using _3.Share.ViewModels.User;

namespace _3.Share.ViewModels.Review;

#pragma warning disable
public class ReviewDto
{
    public string UserId { get; set; }
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public string? Comment { get; set; }
    public int Rating { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    // ref
    public virtual UserFlatRefDto User { get; set; }
    public virtual BookFlatRefDto Book { get; set; }
    public virtual OrderItemFlatRefDto OrderItem { get; set; }
}

/*
PK: UserId BookId OrderItemId
FK: UserId BookId OrderItemId

User không thể bị xóa, chỉ set Status
Book không thể bị xóa, chỉ set Status
OrderItem lúc này không thể bị xóa vì chỉ tạo Review khi OrderItem.Order.Status == Delivered

Add/Update/Delete Review thì Update Book

Rating có thể in range: 1 < Rating < 5 hoặc 2 < Rating < 10

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
