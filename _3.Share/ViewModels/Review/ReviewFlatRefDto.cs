using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.OrderItem;
using _3.Share.ViewModels.User;

namespace _3.Share.ViewModels.Review;

#pragma warning disable
public class ReviewFlatRefDto
{
    public string UserId { get; set; }
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public string? Comment { get; set; }
    public int Rating { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    // ref
    public virtual UserFlatDto User { get; set; }
    public virtual BookFlatDto Book { get; set; }
    public virtual OrderItemFlatDto OrderItem { get; set; }
}
