using _3.Share.ViewModels.Cart;
using _3.Share.ViewModels.Order;
using _3.Share.ViewModels.Review;
using _3.Share.ViewModels.UserCredential;
using _3.Share.ViewModels.Wishlist;

namespace _3.Share.ViewModels.User;

#pragma warning disable
public class UserDto
{
    public string Id { get; set; }
    public string? Avatar { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public bool IsVerified { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    // ghost
    public string DisplayName { get; set; }
    // ref
    public virtual UserCredentialFlatRefDto UserCredential { get; set; }
    public virtual ICollection<CartFlatRefDto> Carts { get; set; }
    public virtual ICollection<OrderFlatRefDto> Orders { get; set; }
    public virtual ICollection<ReviewFlatRefDto> Reviews { get; set; }
    public virtual ICollection<WishlistFlatRefDto> Wishlists { get; set; }
}

/*
User không thể bị xóa, chỉ set DeletedAt bởi chính User hoặc Admin

Cart update Price+Count khi Add/Update/Delete CartItems

OriginalTotalPrice TotalDiscount TotalPrice ItemCount >= 0

Email is Unique & Valid

User cần xác thực Email để IsVerified chuyển sang true

User Valid cần check IsVerified + DelectedAt

UserCredential được tạo sau khi tạo User

Mỗi row trên UserCredential chỉ chính User hoặc Admin mới có quyền truy cập

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/