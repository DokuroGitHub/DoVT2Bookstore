using _3.Share.ViewModels.UserCredential;

namespace _3.Share.ViewModels.User;

#pragma warning disable
public class UserCreateDto
{
    public string? Avatar { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public bool? IsVerified { get; set; }
    public virtual UserCredentialCreateDto? UserCredential { get; set; }
}
