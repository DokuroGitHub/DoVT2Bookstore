using _3.Share.ViewModels.Cart;
using _3.Share.ViewModels.Order;
using _3.Share.ViewModels.Review;
using _3.Share.ViewModels.UserCredential;
using _3.Share.ViewModels.Wishlist;

namespace _3.Share.ViewModels.User;

#pragma warning disable
public class UserFlatRefDto
{
    public string Id { get; set; }
    public string? Avatar { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public bool IsVerified { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    // ghost
    public string DisplayName { get; set; }
    // ref
    public virtual UserCredentialFlatDto UserCredential { get; set; }
    public virtual ICollection<CartFlatDto> Carts { get; set; }
    public virtual ICollection<OrderFlatDto> Orders { get; set; }
    public virtual ICollection<ReviewFlatDto> Reviews { get; set; }
    public virtual ICollection<WishlistFlatDto> Wishlists { get; set; }
}
