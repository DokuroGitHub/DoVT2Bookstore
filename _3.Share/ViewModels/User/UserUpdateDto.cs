namespace _3.Share.ViewModels.User;

#pragma warning disable
public class UserUpdateDto
{
    public string Id { get; set; }
    public string? Avatar { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public bool? IsVerified { get; set; }
}
