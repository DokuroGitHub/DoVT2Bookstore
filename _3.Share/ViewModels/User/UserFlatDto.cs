namespace _3.Share.ViewModels.User;

#pragma warning disable
public class UserFlatDto
{
    public string Id { get; set; }
    public string? Avatar { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string? Email { get; set; }
    public bool IsVerified { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    // ghost
    public string DisplayName { get; set; }
}
