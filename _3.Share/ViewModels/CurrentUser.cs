﻿namespace _3.Share.ViewModels;

#pragma warning disable
public class CurrentUser
{
    public string UserId { get; set; }
    public string DisplayName { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
}
