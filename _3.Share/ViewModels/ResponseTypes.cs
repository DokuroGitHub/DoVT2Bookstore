namespace _3.Share.ViewModels;

#pragma warning disable
public class ResponseTypeId
{
    public string id { get; set; }
}

public class ResponseTypeCartIdBookId
{
    public string cartId { get; set; }
    public string bookId { get; set; }
}

public class ResponseTypeOrderIdBookId
{
    public string orderId { get; set; }
    public string bookId { get; set; }
}

public class ResponseTypeUserId
{
    public string userId { get; set; }
}

public class ResponseTypeWishlistIdBookId
{
    public string wishlistId { get; set; }
    public string bookId { get; set; }
}
