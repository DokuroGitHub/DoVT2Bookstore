using _3.Share.ViewModels.User;

namespace _3.Share.ViewModels.UserCredential;

#pragma warning disable
public class UserCredentialFlatRefDto
{
    public string UserId { get; set; }
    public string Username { get; set; }
    public string HashPassword { get; set; }
    public string Role { get; set; }
    public decimal CreditBalance { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual UserFlatDto User { get; set; }
}
