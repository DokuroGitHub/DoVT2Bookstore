namespace _3.Share.ViewModels.UserCredential;

#pragma warning disable
public class UserCredentialCreateDto
{
    public string UserId { get; set; }
    public string Username { get; set; }
    public string HashPassword { get; set; }
    public string? Role { get; set; }
    public decimal? CreditBalance { get; set; }
}
