using _3.Share.ViewModels.Cart;
using _3.Share.ViewModels.CartItem;
using _3.Share.ViewModels.OrderItem;
using _3.Share.ViewModels.Review;
using _3.Share.ViewModels.WishlistItem;

namespace _3.Share.ViewModels.Book;

#pragma warning disable
public class BookFlatRefDto : BaseEntityDto
{
    public string Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Author { get; set; }
    public string? CoverImage { get; set; }
    public string? Genre { get; set; }
    public decimal Price { get; set; }
    public string Currency { get; set; }
    public int Quantity { get; set; }
    public DateTime PublicationDate { get; set; }
    public float? AvgRating { get; set; }
    // ghost
    public string Status { get; set; }
    // ref
    public virtual ICollection<CartItemFlatDto> CartItems { get; set; }
    public virtual ICollection<OrderItemFlatDto> OrderItems { get; set; }
    public virtual ICollection<ReviewFlatDto> Reviews { get; set; }
    public virtual ICollection<WishlistItemFlatDto> WishlistItems { get; set; }
}
