namespace _3.Share.ViewModels.Book;

public class BookSearchRequest
{
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Author { get; set; }
    public string? Genre { get; set; }
    public DateTime? PublicationDate { get; set; }
}
