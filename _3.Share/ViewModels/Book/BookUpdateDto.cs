namespace _3.Share.ViewModels.Book;

#pragma warning disable
public class BookUpdateDto
{
    public string Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Author { get; set; }
    public string? CoverImage { get; set; }
    public string? Genre { get; set; }
    public decimal? Price { get; set; }
    public string? Currency { get; set; }
    public int? Quantity { get; set; }
    public DateTime? PublicationDate { get; set; }
    public float? AvgRating { get; set; }
    public DateTime? DeletedAt { get; set; }
}
