﻿namespace _3.Share.ViewModels;

#pragma warning disable
public class LoginRequestDto
{
    public string Username { get; set; }
    public string Password { get; set; }
}
