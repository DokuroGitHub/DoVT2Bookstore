using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.Order;
using _3.Share.ViewModels.Review;

namespace _3.Share.ViewModels.OrderItem;

#pragma warning disable
public class OrderItemDto
{
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public OrderFlatRefDto Order { get; set; }
    public BookFlatRefDto Book { get; set; }
    public ReviewFlatRefDto Review { get; set; }
}

/*
Order không thể bị xóa, chỉ set Status

Book không thể bị xóa, chỉ set Status

OrderItem chỉ có thể bị xóa khi Order Status == Unprocessed

Add/Update/Delete OrderItem thì Update Order

Quantity > 0

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
