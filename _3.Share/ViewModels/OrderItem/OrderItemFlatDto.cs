namespace _3.Share.ViewModels.OrderItem;

#pragma warning disable
public class OrderItemFlatDto
{
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}

