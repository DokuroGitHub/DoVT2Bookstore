using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.Order;
using _3.Share.ViewModels.Review;

namespace _3.Share.ViewModels.OrderItem;

#pragma warning disable
public class OrderItemFlatRefDto
{
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public OrderFlatDto Order { get; set; }
    public BookFlatDto Book { get; set; }
    public ReviewFlatDto Review { get; set; }
}
