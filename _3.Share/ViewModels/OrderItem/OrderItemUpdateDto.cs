namespace _3.Share.ViewModels.OrderItem;

#pragma warning disable
public class OrderItemUpdateDto
{
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
}
