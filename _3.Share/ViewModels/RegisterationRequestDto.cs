﻿namespace _3.Share.ViewModels;

#pragma warning disable
public class RegisterationRequestDto
{
    public string Email { get; set; }
    public string Password { get; set; }
    public string ConfirmPassword { get; set; }
    public string? Role { get; set; }
}
