using _3.Share.ViewModels.OrderEvent;
using _3.Share.ViewModels.OrderItem;
using _3.Share.ViewModels.User;

namespace _3.Share.ViewModels.Order;

#pragma warning disable
public class OrderFlatRefDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public decimal OriginalTotalPrice { get; set; } // 2tr
    public decimal TotalDiscount { get; set; } // 100k
    public decimal TotalPrice { get; set; } // 1.9tr + fee => TotalPrice >= OriginalTotalPrice - TotalDiscount
    public string Currency { get; set; }
    public int ItemCount { get; set; }
    public string Status { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual UserFlatDto User { get; set; }
    public virtual ICollection<OrderItemFlatDto> OrderItems { get; set; }
    public virtual ICollection<OrderEventFlatDto> OrderEvents { get; set; }
}
