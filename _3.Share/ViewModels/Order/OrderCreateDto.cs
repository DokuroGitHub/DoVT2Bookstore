using _3.Share.ViewModels.OrderEvent;
using _3.Share.ViewModels.OrderItem;

namespace _3.Share.ViewModels.Order;

#pragma warning disable
public class OrderCreateDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public decimal? OriginalTotalPrice { get; set; } // 2tr
    public decimal? TotalDiscount { get; set; } // 100k
    public decimal? TotalPrice { get; set; } // 1.9tr + fee => TotalPrice >= OriginalTotalPrice - TotalDiscount
    public string? Currency { get; set; }
    public int? ItemCount { get; set; }
    public string? Status { get; set; }
    // ref
    public virtual ICollection<OrderItemCreateDto>? OrderItems { get; set; }
    public virtual ICollection<OrderEventCreateDto>? OrderEvents { get; set; }
}
