namespace _3.Share.ViewModels.Order;

#pragma warning disable
public class OrderUpdateDto
{
    public string Id { get; set; }
    public decimal? OriginalTotalPrice { get; set; } // 2tr
    public decimal? TotalDiscount { get; set; } // 100k
    public decimal? TotalPrice { get; set; } // 1.9tr + fee => TotalPrice >= OriginalTotalPrice - TotalDiscount
    public string? Currency { get; set; }
    public int? ItemCount { get; set; }
    public string? Status { get; set; }
}
