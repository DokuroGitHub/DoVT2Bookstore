namespace _3.Share.ViewModels.Order;

#pragma warning disable
public class OrderFlatDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public decimal OriginalTotalPrice { get; set; } // 2tr
    public decimal TotalDiscount { get; set; } // 100k
    public decimal TotalPrice { get; set; } // 1.9tr + fee => TotalPrice >= OriginalTotalPrice - TotalDiscount
    public string Currency { get; set; }
    public int ItemCount { get; set; }
    public string Status { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}

