using _3.Share.ViewModels.CartItem;
using _3.Share.ViewModels.User;

namespace _3.Share.ViewModels.Cart;

#pragma warning disable
public class CartDto : BaseEntityDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public decimal OriginalTotalPrice { get; set; }
    public decimal TotalDiscount { get; set; }
    public decimal TotalPrice { get; set; }
    public string Currency { get; set; }
    public int ItemCount { get; set; }
    // ref
    public virtual UserFlatRefDto User { get; set; }
    public virtual ICollection<CartItemFlatRefDto> CartItems { get; set; }
}

/*
1 User có thể có nhiều Cart (multi-cart) hoặc chỉ 1 cart (single-cart)

Cart có thể bị xóa khi người dùng anonymous có CartItems login vào hệ thống mà đã có Cart

Delete Cart thì Delete cascade CartItems

Cart update Price+Count khi Add/Update/Delete CartItems

OriginalTotalPrice TotalDiscount TotalPrice ItemCount >= 0

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
