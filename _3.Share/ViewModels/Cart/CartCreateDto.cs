using _3.Share.ViewModels.CartItem;

namespace _3.Share.ViewModels.Cart;

#pragma warning disable
public class CartCreateDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public decimal? OriginalTotalPrice { get; set; }
    public decimal? TotalDiscount { get; set; }
    public decimal? TotalPrice { get; set; }
    public string? Currency { get; set; }
    public int? ItemCount { get; set; }
    // ref
    public virtual ICollection<CartItemCreateDto>? CartItems { get; set; }
}
