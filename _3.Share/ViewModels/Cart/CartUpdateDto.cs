namespace _3.Share.ViewModels.Cart;

#pragma warning disable
public class CartUpdateDto
{
    public string Id { get; set; }
    public string? Currency { get; set; }
}
