using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.Wishlist;

namespace _3.Share.ViewModels.WishlistItem;

#pragma warning disable
public class WishlistItemDto
{
    public string WishlistId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual WishlistFlatRefDto Wishlist { get; set; }
    public virtual BookFlatRefDto Book { get; set; }
}

/*
PR: WishlistId BookId

Book không thẻ bị xóa, chỉ set DeletedAt
Wishlist có thể bị xóa
WishlistItem có thể bị xóa, Cascade khi xóa Wishlist 

Quantity > 0, Quantity = 0 thì xóa
Create/Update/Delete WishlistItem thì Update Wishlist

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
