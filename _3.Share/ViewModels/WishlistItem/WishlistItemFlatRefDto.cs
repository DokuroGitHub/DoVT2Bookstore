using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.Wishlist;

namespace _3.Share.ViewModels.WishlistItem;

#pragma warning disable
public class WishlistItemFlatRefDto
{
    public string WishlistId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual WishlistFlatDto Wishlist { get; set; }
    public virtual BookFlatDto Book { get; set; }
}
