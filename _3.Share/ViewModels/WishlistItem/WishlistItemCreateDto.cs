namespace _3.Share.ViewModels.WishlistItem;

#pragma warning disable
public class WishlistItemCreateDto
{
    public string WishlistId { get; set; }
    public string BookId { get; set; }
    public int? Quantity { get; set; }
}
