namespace _3.Share.ViewModels.WishlistItem;

#pragma warning disable
public class WishlistItemFlatDto
{
    public string WishlistId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}

