using _3.Share.ViewModels.User;
using _3.Share.ViewModels.WishlistItem;

namespace _3.Share.ViewModels.Wishlist;

#pragma warning disable
public class WishlistFlatRefDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public string? Description { get; set; }
    public int ItemCount { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual UserFlatDto User { get; set; }
    public virtual ICollection<WishlistItemFlatDto> WishlistItems { get; set; }
}
