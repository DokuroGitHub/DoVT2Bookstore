namespace _3.Share.ViewModels.Wishlist;

#pragma warning disable
public class WishlistUpdateDto
{
    public string Id { get; set; }
    public string? Description { get; set; }
    public int ItemCount { get; set; }
}
