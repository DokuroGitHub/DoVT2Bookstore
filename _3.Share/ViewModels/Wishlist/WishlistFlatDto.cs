namespace _3.Share.ViewModels.Wishlist;

#pragma warning disable
public class WishlistFlatDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public string? Description { get; set; }
    public int ItemCount { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}
