using _3.Share.ViewModels.WishlistItem;

namespace _3.Share.ViewModels.Wishlist;

#pragma warning disable
public class WishlistCreateDto
{
    public string UserId { get; set; }
    public string? Description { get; set; }
    public int? ItemCount { get; set; }
    // ref
    public virtual ICollection<WishlistItemCreateDto>? WishlistItems { get; set; }
}
