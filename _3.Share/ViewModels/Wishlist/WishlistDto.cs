using _3.Share.ViewModels.User;
using _3.Share.ViewModels.WishlistItem;

namespace _3.Share.ViewModels.Wishlist;

#pragma warning disable
public class WishlistDto
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public string? Description { get; set; }
    public int ItemCount { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual UserFlatRefDto User { get; set; }
    public virtual ICollection<WishlistItemFlatRefDto> WishlistItems { get; set; }
}

/*
Wishlist có thể bị xóa, chia sẻ
User không thẻ bị xóa, chỉ set DeletedAt
User có nhiều Wishlist

ItemCount >= 0, auto update khi WishlistItem Create/Update/Delete

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/