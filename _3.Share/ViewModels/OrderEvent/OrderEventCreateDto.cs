namespace _3.Share.ViewModels.OrderEvent;

#pragma warning disable
public class OrderEventCreateDto
{
    public string Id { get; set; }
    public string OrderId { get; set; }
    public string? Event { get; set; }
    public string? Description { get; set; }
}
