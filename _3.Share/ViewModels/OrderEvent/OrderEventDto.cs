using _3.Share.ViewModels.Order;

namespace _3.Share.ViewModels.OrderEvent;

#pragma warning disable
public class OrderEventDto
{
    public string Id { get; set; }
    public string OrderId { get; set; }
    public string Event { get; set; }
    public string? Description { get; set; }
    public DateTime CreatedAt { get; set; }
    // ref
    public virtual OrderFlatRefDto Order { get; set; }
}

/*
Order không thể bị xóa, chỉ set Status

OrderEvents không thể bị xóa, OrderEvents ghi lịch sử cho Order

Event là Enum code cho sự kiện

CreatedAt auto trigger on row Create

*/
