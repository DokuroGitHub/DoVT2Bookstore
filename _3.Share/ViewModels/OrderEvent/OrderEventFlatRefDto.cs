using _3.Share.ViewModels.Order;

namespace _3.Share.ViewModels.OrderEvent;

#pragma warning disable
public class OrderEventFlatRefDto
{
    public string Id { get; set; }
    public string OrderId { get; set; }
    public string Event { get; set; }
    public string? Description { get; set; }
    public DateTime CreatedAt { get; set; }
    // ref
    public virtual OrderFlatDto Order { get; set; }
}
