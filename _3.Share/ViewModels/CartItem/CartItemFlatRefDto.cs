using _3.Share.ViewModels.Book;
using _3.Share.ViewModels.Cart;

namespace _3.Share.ViewModels.CartItem;

#pragma warning disable
public class CartItemFlatRefDto
{
    public string CartId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual CartFlatDto Cart { get; set; }
    public virtual BookFlatDto Book { get; set; }
}
