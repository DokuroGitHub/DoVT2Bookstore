namespace _3.Share.ViewModels.CartItem;

#pragma warning disable
public class CartItemUpdateDto
{
    public string CartId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
}
