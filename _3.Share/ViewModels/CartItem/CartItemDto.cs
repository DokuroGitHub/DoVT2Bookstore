using _3.Share.ViewModels.Cart;
using _3.Share.ViewModels.Book;

namespace _3.Share.ViewModels.CartItem;

#pragma warning disable
public class CartItemDto
{
    public string CartId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    // ref
    public virtual CartFlatRefDto Cart { get; set; }
    public virtual BookFlatRefDto Book { get; set; }
}

/*
CartItem bị xóa khi Delete CartItem hoặc Delete Cart, Book không Delete được

Add/Update/Delete CartItem thì Update Cart

Quantity > 0

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
