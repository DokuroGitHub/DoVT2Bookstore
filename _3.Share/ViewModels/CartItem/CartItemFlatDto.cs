namespace _3.Share.ViewModels.CartItem;

#pragma warning disable
public class CartItemFlatDto
{
    public string CartId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}
