﻿namespace _3.Share.ViewModels;

#pragma warning disable
public class LoginResponseDto
{
    public CurrentUser CurrentUser { get; set; }
    public string Token { get; set; }
}
