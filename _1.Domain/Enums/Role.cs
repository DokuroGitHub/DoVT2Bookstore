namespace _1.Domain.Enums;

public enum Role
{
    Admin,
    User,
    Guest
}
