namespace _1.Domain.Enums;

public enum OrderStatus
{
    Delivered,
    Processing,
    Shipped
}
