namespace _1.Domain.Entities;

#pragma warning disable
public class Wishlist : BaseEntity
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public string? Description { get; set; }
    public int ItemCount { get; set; }
    // ref
    public virtual User User { get; set; }
    public virtual ICollection<WishlistItem> WishlistItems { get; set; }
}

/*
Wishlist có thể bị xóa, chia sẻ
User không thẻ bị xóa, chỉ set DeletedAt
User có nhiều Wishlist

ItemCount >= 0, auto update khi WishlistItem Create/Update/Delete

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/