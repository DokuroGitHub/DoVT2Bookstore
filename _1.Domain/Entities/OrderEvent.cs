namespace _1.Domain.Entities;

#pragma warning disable
public class OrderEvent : BaseEntity
{
    public string Id { get; set; }
    public string OrderId { get; set; }
    public string Event { get; set; }
    public string? Description { get; set; }
    // ref
    public virtual Order Order { get; set; }
}

/*
Order không thể bị xóa, chỉ set Status

OrderEvents không thể bị xóa, OrderEvents ghi lịch sử cho Order

Event là Enum code cho sự kiện

CreatedAt auto trigger on row Create

*/
