namespace _1.Domain.Entities;

#pragma warning disable
public class WishlistItem : BaseEntity
{
    public string WishlistId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    // ref
    public virtual Wishlist Wishlist { get; set; }
    public virtual Book Book { get; set; }
}

/*
PR: WishlistId BookId

Book không thẻ bị xóa, chỉ set DeletedAt
Wishlist có thể bị xóa
WishlistItem có thể bị xóa, Cascade khi xóa Wishlist 

Quantity > 0, Quantity = 0 thì xóa
Create/Update/Delete WishlistItem thì Update Wishlist

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
