namespace _1.Domain.Entities;

#pragma warning disable
public class Book : BaseEntity
{
    public string Id { get; set; }
    public string? Title { get; set; }
    public string? Description { get; set; }
    public string? Author { get; set; }
    public string? CoverImage { get; set; }
    public string? Genre { get; set; }
    public decimal Price { get; set; }
    public string Currency { get; set; }
    public int Quantity { get; set; }
    public DateTime PublicationDate { get; set; }
    public float? AvgRating { get; set; }
    // ghost
    public string Status { get; set; }
    // ref
    public virtual ICollection<CartItem> CartItems { get; set; }
    public virtual ICollection<OrderItem> OrderItems { get; set; }
    public virtual ICollection<Review> Reviews { get; set; }
    public virtual ICollection<WishlistItem> WishlistItems { get; set; }
}

/*
Book ko dc xóa, chỉ set DeletedAt

Quantity, Price >= 0

AvgRating auto update khi Review Add/Update/set DeletedAt

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
