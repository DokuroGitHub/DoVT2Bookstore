namespace _1.Domain.Entities;

#pragma warning disable
public class CartItem : BaseEntity
{
    public string CartId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    // ref
    public virtual Cart Cart { get; set; }
    public virtual Book Book { get; set; }
}

/*
CartItem bị xóa khi Delete CartItem hoặc Delete Cart, Book không Delete được

Add/Update/Delete CartItem thì Update Cart

Quantity > 0

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
