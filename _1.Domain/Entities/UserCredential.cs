namespace _1.Domain.Entities;

#pragma warning disable
public class UserCredential : BaseEntity
{
    public string UserId { get; set; }
    public string Username { get; set; }
    public string HashPassword { get; set; }
    public string Role { get; set; }
    public decimal CreditBalance { get; set; }
    // ref
    public virtual User User { get; set; }
}

/*
User không thể bị xóa, chỉ set DeletedAt

UserCredential không thể bị xóa

Username mặc định là Email, nếu Email null thì là UserId

Email is Unique

CreditBalance >= 0

Role chỉ Admin có quyền chỉnh sửa

UpdatedAt auto trigger on row update

*/