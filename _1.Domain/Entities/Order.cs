namespace _1.Domain.Entities;

#pragma warning disable
public class Order : BaseEntity
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public decimal OriginalTotalPrice { get; set; } // 2tr
    public decimal TotalDiscount { get; set; } // 100k
    public decimal TotalPrice { get; set; } // 1.9tr + fee => TotalPrice >= OriginalTotalPrice - TotalDiscount
    public string Currency { get; set; }
    public int ItemCount { get; set; }
    public string Status { get; set; }
    // ref
    public virtual User User { get; set; }
    public virtual ICollection<OrderItem> OrderItems { get; set; }
    public virtual ICollection<OrderEvent> OrderEvents { get; set; }
}

/*
Order không thể bị xóa, chỉ set Status, OrderEvents ghi lịch sử cho Order

OriginalTotalPrice TotalDiscount TotalPrice >= 0

ItemCount > 0

Cart update Price+Count khi Add/Update/Delete CartItems

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
