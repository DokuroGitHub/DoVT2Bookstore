namespace _1.Domain.Entities;

#pragma warning disable
public class OrderItem : BaseEntity
{
    public string OrderId { get; set; }
    public string BookId { get; set; }
    public int Quantity { get; set; }
    // ref
    public Order Order { get; set; }
    public Book Book { get; set; }
    public Review Review { get; set; }
}

/*
Order không thể bị xóa, chỉ set Status

Book không thể bị xóa, chỉ set Status

OrderItem chỉ có thể bị xóa khi Order Status == Unprocessed

Add/Update/Delete OrderItem thì Update Order

Quantity > 0

CreatedAt auto trigger on row Create
UpdatedAt auto trigger on row Update

*/
