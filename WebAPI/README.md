# Infrastructure

```bash
dotnet new webapi

dotnet add reference "../Infrastructure/Infrastructure.csproj"
#
dotnet build
#
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package AutoMapper
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.AspNetCore.Mvc.Versionin
dotnet add package Microsoft.AspNetCore.Mvc.Versioning.ApiExplorer
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
dotnet add package Microsoft.EntityFrameworkCore.InMemory
dotnet add package FluentValidation
dotnet add package FluentValidation.AspNetCore
dotnet add package Swashbuckle.AspNetCore
#

dotnet add package Microsoft.EntityFrameworkCore.Proxies
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson
#
```
