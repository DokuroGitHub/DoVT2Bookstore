using WebAPI.Middlewares;
using WebAPI.ServicesExtensions;
using Infrastructure;

var builder = WebApplication.CreateBuilder(args);

//* Add services to the container.
// appsettings.json
var _appsettings = builder.Configuration.Get<Appsettings>() ?? new Appsettings();
// services
builder.Services.AddInfrastructureServices(_appsettings);
builder.Services.AddWebApiServices(_appsettings);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("v1/swagger.json", "DoVT2Bookstore v1");
        options.SwaggerEndpoint("v2/swagger.json", "DoVT2Bookstore v2");
    });
}
app.UseExceptionMiddleware();
app.UsePerformanceMiddleware();
app.MapHealthChecks("/healthchecks");
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();

Console.WriteLine("hello there");

// this line tell intergrasion test
// https://stackoverflow.com/questions/69991983/deps-file-missing-for-dotnet-6-integration-tests
public partial class Program { }