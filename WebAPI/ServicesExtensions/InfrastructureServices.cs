﻿using Application.IServices;
using Domain2Db;
using Infrastructure;
using Infrastructure.Repositories;
using Infrastructure.Repositories.IRepositories;
using Infrastructure.Services;
using Infrastructure.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.ServicesExtensions;

public static class InfrastructureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, Appsettings appsettings)
    {
        // mapper
        services.AddAutoMapper(typeof(MappingConfig).Assembly);

        // Repositories
        services.AddScoped<IBookRepository, BookRepository>();
        services.AddScoped<ICartRepository, CartRepository>();
        services.AddScoped<IUserCredentialRepository, UserCredentialRepository>();
        services.AddScoped<IUserRepository, UserRepository>();

        // UnitOfWork
        services.AddScoped<IUnitOfWork, UnitOfWork>();

        // add services
        services.AddScoped<IAuthService, AuthService>();
        services.AddScoped<IBookService, BookService>();
        services.AddScoped<ICurrentTimeService, CurrentTimeService>();
        services.AddScoped<ICurrentUserService, CurrentUserService>();
        services.AddScoped<IJwtService, JwtService>();
        services.AddHttpClient<IMailService, MailService>();
        services.AddScoped<IMailService, MailService>();

        // DbContext
        services.AddDbContext<ApplicationDbContext>(option => option.UseSqlServer(appsettings.ConnectionStrings.DokuroSQLConnectionString));
        // services.AddDbContext<ApplicationDbContext>(option => option.UseInMemoryDatabase("DokuroDb"));
        services.AddMemoryCache();

        return services;
    }
}
