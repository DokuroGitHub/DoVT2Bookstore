using Infrastructure.Services.IServices;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers.v2;

[ApiController]
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/[controller]")]
public class MailController : ControllerBase
{
    private readonly IMailService _mailService;

    public MailController(IMailService mailService)
    {
        _mailService = mailService;
    }

    [HttpPost("send-mail")]
    public async Task<IActionResult> SendMailAsync([FromBody] List<string> emails, string subject, string content)
    {
        await _mailService.SendMailAsync(emails, subject, content);
        return Ok();
    }
}
