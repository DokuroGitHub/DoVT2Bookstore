using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.CartItem;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace _4.BusinessLayer.Services;

public class CartItemService : ICartItemService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _currentUserService;

    public CartItemService(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICurrentUserService currentUserService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _currentUserService = currentUserService;
    }

    public async Task<PagedResult<CartItemDto>> GetMyCartItems(int pageIndex, int pageSize)
    {
        var result = await _unitOfWork.CartItemRepository.GetPagedResultAsync(
            filter: x => x.Cart.UserId == _currentUserService.CurrentUserId,
            include: x => x.Include(x => x.Book),
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<CartItemDto>>(result);
        return pagedItems;
    }

    public async Task<CartItemDto?> GetOne(string cartId, string bookId)
    {
        var item = await _unitOfWork.CartItemRepository.FirstOrDefaultAsync(
            filter: x => x.CartId == cartId && x.BookId == bookId &&
                (_currentUserService.IsAdmin || x.Cart.UserId == _currentUserService.CurrentUserId),
            include: x => x.Include(x => x.Book));
        if (item == null)
            return null;
        var result = _mapper.Map<CartItemDto>(item);
        return result;
    }

    public async Task<CartItemDto> Create(CartItemCreateDto dto)
    {
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == dto.BookId);
        if (book == null)
            throw new ServiceException("Not found the book");
        dto.Quantity = dto.Quantity > 0 ? dto.Quantity : 1;
        dto.Quantity = dto.Quantity > book.Quantity ? book.Quantity : dto.Quantity;
        try
        {
            _unitOfWork.BeginTransaction();
            var cartItem = _mapper.Map<CartItem>(dto);
            if (dto.CartId == null)
            {
                var cart = await _unitOfWork.CartRepository.FirstOrDefaultAsync(
                    filter: x => x.UserId == _currentUserService.CurrentUserId);
                if (cart == null)
                {
                    cartItem.Cart = new Cart
                    {
                        UserId = _currentUserService.CurrentUserId,
                        CreatedBy = _currentUserService.CurrentUserId,
                        OriginalTotalPrice = book.Price * (dto.Quantity ?? 1),
                        TotalDiscount = 0,
                        TotalPrice = book.Price * (dto.Quantity ?? 1),
                        ItemCount = dto.Quantity ?? 1
                    };
                    await _unitOfWork.CartItemRepository.AddAsync(cartItem);
                    await _unitOfWork.CommitAsync();
                    return _mapper.Map<CartItemDto>(cartItem);
                }
                cartItem.CartId = cart.Id;
                await _unitOfWork.CartItemRepository.AddAsync(cartItem);
                await _unitOfWork.SaveChangesAsync();
                await UpdateCartPrices(cart);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<CartItemDto>(cartItem);
            }
            await _unitOfWork.CartItemRepository.AddAsync(cartItem);
            await _unitOfWork.SaveChangesAsync();
            await UpdateCartPrices(cartItem.CartId);
            await _unitOfWork.CommitAsync();
            var result = _mapper.Map<CartItemDto>(cartItem);
            return result;
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't add cart item, {ex.Message}", ex);
        }
    }

    public async Task<ResponseTypeCartIdBookId> Delete(string cartId, string bookId)
    {
        var item = await _unitOfWork.CartItemRepository.FirstOrDefaultAsync(
            filter: x => x.CartId == cartId && x.BookId == bookId &&
                (_currentUserService.IsAdmin || x.Cart.UserId == _currentUserService.CurrentUserId));
        if (item == null)
            throw new ServiceException("Not found the cart item");
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.CartItemRepository.Remove(item);
            await _unitOfWork.SaveChangesAsync();
            await UpdateCartPrices(item.CartId);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete cart item", ex);
        }
        var result = _mapper.Map<CartItemDto>(item);
        return new ResponseTypeCartIdBookId { cartId = result.CartId, bookId = result.BookId };
    }

    public async Task<CartItemDto> Update(string cartId, string bookId, CartItemUpdateDto dto)
    {
        dto.CartId = cartId;
        dto.BookId = bookId;
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == dto.BookId);
        if (book == null)
            throw new ServiceException("Not found the book");
        var item = await _unitOfWork.CartItemRepository.FirstOrDefaultAsync(
            filter: x => x.CartId == dto.CartId && x.BookId == dto.BookId &&
                (_currentUserService.IsAdmin || x.Cart.UserId == _currentUserService.CurrentUserId));
        if (item == null)
            throw new ServiceException("Not found the cart item");
        item.Quantity = dto.Quantity > 0 ? dto.Quantity : 1;
        item.Quantity = dto.Quantity > book.Quantity ? book.Quantity : dto.Quantity;
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.CartItemRepository.Update(item);
            await _unitOfWork.SaveChangesAsync();
            await UpdateCartPrices(dto.CartId);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't update cart, {ex.Message}", ex);
        }
        var result = _mapper.Map<CartItemDto>(item);
        return result;
    }

    //    
    private async Task UpdateCartPrices(string id)
    {
        var cart = await _unitOfWork.CartRepository.FirstOrDefaultAsync(
            filter: x => x.Id == id,
            include: x => x.Include(x => x.CartItems).ThenInclude(x => x.Book));
        if (cart == null)
            throw new ServiceException("Not found the cart");
        var cartItems = cart.CartItems;
        cart.CartItems = new List<CartItem>();

        cart.ItemCount = cartItems.Sum(x => x.Quantity);
        cart.OriginalTotalPrice = cartItems.Sum(x => x.Quantity * x.Book.Price);
        cart.TotalDiscount = 0;
        cart.TotalPrice = cartItems.Sum(x => x.Quantity * x.Book.Price);
        _unitOfWork.CartRepository.Update(cart);
    }

    private async Task UpdateCartPrices(Cart cart)
    {
        var cartItems = await _unitOfWork.CartItemRepository.GetAllAsync(
            filter: x => x.CartId == cart.Id,
            include: x => x.Include(x => x.Book));
        cart.ItemCount = cartItems.Sum(x => x.Quantity);
        cart.OriginalTotalPrice = cartItems.Sum(x => x.Quantity * x.Book.Price);
        cart.TotalDiscount = 0;
        cart.TotalPrice = cartItems.Sum(x => x.Quantity * x.Book.Price);
        _unitOfWork.CartRepository.Update(cart);
    }
}
