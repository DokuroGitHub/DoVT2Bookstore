using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.OrderItem;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace _4.BusinessLayer.Services;

public class OrderItemService : IOrderItemService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _currentUserService;

    public OrderItemService(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICurrentUserService currentUserService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _currentUserService = currentUserService;
    }

    public async Task<PagedResult<OrderItemDto>> GetMyOrderItems(int pageIndex, int pageSize)
    {
        var result = await _unitOfWork.OrderItemRepository.GetPagedResultAsync(
            filter: x => x.Order.UserId == _currentUserService.CurrentUserId,
            include: x => x.Include(x => x.Book),
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<OrderItemDto>>(result);
        return pagedItems;
    }

    public async Task<OrderItemDto?> GetOne(string orderId, string bookId)
    {
        var item = await _unitOfWork.OrderItemRepository.FirstOrDefaultAsync(
            filter: x => x.OrderId == orderId && x.BookId == bookId &&
                (_currentUserService.IsAdmin || x.Order.UserId == _currentUserService.CurrentUserId),
            include: x => x.Include(x => x.Book));
        if (item == null)
            return null;
        var result = _mapper.Map<OrderItemDto>(item);
        return result;
    }

    public async Task<OrderItemDto> Create(OrderItemCreateDto dto)
    {
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == dto.BookId);
        if (book == null)
            throw new ServiceException("Not found the book");
        dto.Quantity = dto.Quantity > 0 ? dto.Quantity : 1;
        dto.Quantity = dto.Quantity > book.Quantity ? book.Quantity : dto.Quantity;
        try
        {
            _unitOfWork.BeginTransaction();
            var orderItem = _mapper.Map<OrderItem>(dto);
            if (dto.OrderId == null)
            {
                var order = await _unitOfWork.OrderRepository.FirstOrDefaultAsync(
                    filter: x => x.UserId == _currentUserService.CurrentUserId);
                if (order == null)
                {
                    orderItem.Order = new Order
                    {
                        UserId = _currentUserService.CurrentUserId,
                        CreatedBy = _currentUserService.CurrentUserId,
                        OriginalTotalPrice = book.Price * (dto.Quantity ?? 1),
                        TotalDiscount = 0,
                        TotalPrice = book.Price * (dto.Quantity ?? 1),
                        ItemCount = dto.Quantity ?? 1
                    };
                    await _unitOfWork.OrderItemRepository.AddAsync(orderItem);
                    await _unitOfWork.CommitAsync();
                    return _mapper.Map<OrderItemDto>(orderItem);
                }
                orderItem.OrderId = order.Id;
                await _unitOfWork.OrderItemRepository.AddAsync(orderItem);
                await _unitOfWork.SaveChangesAsync();
                await UpdateOrderPrices(order);
                await _unitOfWork.CommitAsync();
                return _mapper.Map<OrderItemDto>(orderItem);
            }
            await _unitOfWork.OrderItemRepository.AddAsync(orderItem);
            await _unitOfWork.SaveChangesAsync();
            await UpdateOrderPrices(orderItem.OrderId);
            await _unitOfWork.CommitAsync();
            var result = _mapper.Map<OrderItemDto>(orderItem);
            return result;
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't add order item, {ex.Message}", ex);
        }
    }

    public async Task<ResponseTypeOrderIdBookId> Delete(string orderId, string bookId)
    {
        var item = await _unitOfWork.OrderItemRepository.FirstOrDefaultAsync(
            filter: x => x.OrderId == orderId && x.BookId == bookId &&
                (_currentUserService.IsAdmin || x.Order.UserId == _currentUserService.CurrentUserId));
        if (item == null)
            throw new ServiceException("Not found the order item");
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.OrderItemRepository.Remove(item);
            await _unitOfWork.SaveChangesAsync();
            await UpdateOrderPrices(item.OrderId);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete order item", ex);
        }
        var result = _mapper.Map<OrderItemDto>(item);
        return new ResponseTypeOrderIdBookId { orderId = result.OrderId, bookId = result.BookId };
    }

    public async Task<OrderItemDto> Update(string orderId, string bookId, OrderItemUpdateDto dto)
    {
        dto.OrderId = orderId;
        dto.BookId = bookId;
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == dto.BookId);
        if (book == null)
            throw new ServiceException("Not found the book");
        var item = await _unitOfWork.OrderItemRepository.FirstOrDefaultAsync(
            filter: x => x.OrderId == dto.OrderId && x.BookId == dto.BookId &&
                (_currentUserService.IsAdmin || x.Order.UserId == _currentUserService.CurrentUserId));
        if (item == null)
            throw new ServiceException("Not found the order item");
        item.Quantity = dto.Quantity > 0 ? dto.Quantity : 1;
        item.Quantity = dto.Quantity > book.Quantity ? book.Quantity : dto.Quantity;
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.OrderItemRepository.Update(item);
            await _unitOfWork.SaveChangesAsync();
            await UpdateOrderPrices(dto.OrderId);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't update order, {ex.Message}", ex);
        }
        var result = _mapper.Map<OrderItemDto>(item);
        return result;
    }

    //    
    private async Task UpdateOrderPrices(string id)
    {
        var order = await _unitOfWork.OrderRepository.FirstOrDefaultAsync(
            filter: x => x.Id == id,
            include: x => x.Include(x => x.OrderItems).ThenInclude(x => x.Book));
        if (order == null)
            throw new ServiceException("Not found the order");
        var orderItems = order.OrderItems;
        order.OrderItems = new List<OrderItem>();

        order.ItemCount = orderItems.Sum(x => x.Quantity);
        order.OriginalTotalPrice = orderItems.Sum(x => x.Quantity * x.Book.Price);
        order.TotalDiscount = 0;
        order.TotalPrice = orderItems.Sum(x => x.Quantity * x.Book.Price);
        _unitOfWork.OrderRepository.Update(order);
    }

    private async Task UpdateOrderPrices(Order order)
    {
        var orderItems = await _unitOfWork.OrderItemRepository.GetAllAsync(
            filter: x => x.OrderId == order.Id,
            include: x => x.Include(x => x.Book));
        order.ItemCount = orderItems.Sum(x => x.Quantity);
        order.OriginalTotalPrice = orderItems.Sum(x => x.Quantity * x.Book.Price);
        order.TotalDiscount = 0;
        order.TotalPrice = orderItems.Sum(x => x.Quantity * x.Book.Price);
        _unitOfWork.OrderRepository.Update(order);
    }
}
