using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.Order;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace _4.BusinessLayer.Services;

public class OrderService : IOrderService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _currentUserService;

    public OrderService(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICurrentUserService currentUserService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _currentUserService = currentUserService;
    }

    public async Task<PagedResult<OrderDto>> GetMyOrders(int pageIndex, int pageSize)
    {
        var result = await _unitOfWork.OrderRepository.GetPagedResultAsync(
            filter: x => x.UserId == _currentUserService.CurrentUserId,
            include: x => x.Include(x => x.OrderItems).ThenInclude(x => x.Book),
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<OrderDto>>(result);
        return pagedItems;
    }

    public async Task<OrderDto?> GetOne(string id)
    {
        var item = await _unitOfWork.OrderRepository.FirstOrDefaultAsync(
            filter: x => x.Id == id &&
                (_currentUserService.IsAdmin || x.UserId == _currentUserService.CurrentUserId));
        if (item == null)
            return null;
        var result = _mapper.Map<OrderDto>(item);
        return result;
    }

    public async Task<OrderDto> Create(OrderCreateDto dto)
    {
        var order = _mapper.Map<Order>(dto);
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.OrderRepository.AddAsync(order);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't add order, {ex.Message}", ex);
        }
        var result = _mapper.Map<OrderDto>(order);
        return result;
    }

    public async Task<ResponseTypeId> Delete(string id)
    {
        var item = await _unitOfWork.OrderRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (item == null)
            throw new ServiceException("Not found the order");
        if (!_currentUserService.IsAdmin && _currentUserService.CurrentUserId != item.UserId)
            throw new ServiceException("You don't have permission to delete this order");
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.OrderItemRepository.RemoveRangeAsync(x => x.OrderId == item.Id);
            _unitOfWork.OrderRepository.Remove(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete order", ex);
        }
        var result = _mapper.Map<OrderDto>(item);
        return new ResponseTypeId { id = result.Id };
    }

    public async Task<OrderDto> Update(string id, OrderUpdateDto dto)
    {
        dto.Id = id;
        var item = await _unitOfWork.OrderRepository.FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (item == null)
            throw new ServiceException("Not found the order");
        if (!_currentUserService.IsAdmin && _currentUserService.CurrentUserId != item.UserId)
            throw new ServiceException("You don't have permission to update this order");
        item.Currency = dto.Currency ?? item.Currency;
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.OrderRepository.Update(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't update order, {ex.Message}", ex);
        }
        var result = _mapper.Map<OrderDto>(item);
        return result;
    }

}
