using System.Linq.Expressions;
using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.Book;
using _4.BusinessLayer.Services.IServices;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace _4.BusinessLayer.Services;

public class BookService : IBookService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IMailService _emailService;
    private readonly ICurrentUserService _currentUserService;

    public BookService(
        IUnitOfWork unitOfWork,
        IMailService emailService,
        IMapper mapper,
        ICurrentUserService currentUserService)
    {
        _unitOfWork = unitOfWork;
        _emailService = emailService;
        _mapper = mapper;
        _currentUserService = currentUserService;
    }

    public async Task<PagedResult<BookDto>> GetPagedItems(
        string? search = null,
        string? includeProperties = "reviews,orderItems",
        string? selectFields = "id,title,description",
        int pageIndex = 1,
        int pageSize = 10)
    {
        includeProperties = includeProperties?.ToLower();
        string[] includeList = includeProperties?.Split(',') ?? new string[0];
        Func<IQueryable<Book>, IQueryable<Book>> include = x =>
        {
            if (includeList.Length == 0)
                return x;
            if (includeList.Contains("reviews"))
            {
                x = x.Include(x => x.Reviews);
                if (includeList.Contains("reviews.user"))
                {
                    x = x.Include(x => x.Reviews).ThenInclude(x => x.User);
                }
            }
            if (includeList.Contains("orderitems"))
            {
                x = x.Include(x => x.OrderItems);
                if (includeList.Contains("orderitems.order"))
                {
                    x = x.Include(x => x.OrderItems).ThenInclude(x => x.Order);
                }
                if (includeList.Contains("orderitems.review"))
                {
                    x = x.Include(x => x.OrderItems).ThenInclude(x => x.Review);
                }
            }
            return x;
        };

        var result = await _unitOfWork.BookRepository.GetPagedResultAsync(
            filter: x => string.IsNullOrEmpty(search) ||
                (x.Title != null && x.Title.Contains(search)) ||
                (x.Description != null && x.Description.Contains(search)) ||
                (x.Author != null && x.Author.Contains(search)),
            include: include,
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<BookDto>>(result);
        return pagedItems;
    }

    public async Task<PagedResult<BookDto>> AdvancedSearch(
        string? title = null,
        string? description = null,
        string? author = null,
        string? genre = null,
        string? status = null,
        string? includeProperties = "reviews,orderItems",
        int pageIndex = 1,
        int pageSize = 10)
    {
        Expression<Func<Book, bool>> filter = x => true;
        filter = string.IsNullOrEmpty(title) ?
            filter :
            filter.AndAlso(x => x.Title != null && x.Title.Contains(title));

        filter = string.IsNullOrEmpty(description) ?
            filter :
            filter.AndAlso(x => x.Description != null && x.Description.Contains(description));

        filter = string.IsNullOrEmpty(author) ?
            filter :
            filter.AndAlso(x => x.Author != null && x.Author.Contains(author));

        filter = string.IsNullOrEmpty(genre) ?
            filter :
            filter.AndAlso(x => x.Genre != null && x.Genre.Contains(genre));

        filter = string.IsNullOrEmpty(status) ?
            filter :
            filter.AndAlso(x => x.Status == status);

        includeProperties = includeProperties?.ToLower();
        string[] includeList = includeProperties?.Split(',') ?? new string[0];
        Func<IQueryable<Book>, IQueryable<Book>> include = x =>
        {
            if (includeList.Length == 0)
                return x;
            if (includeList.Contains("reviews"))
            {
                x = x.Include(x => x.Reviews);
                if (includeList.Contains("reviews.user"))
                {
                    x = x.Include(x => x.Reviews).ThenInclude(x => x.User);
                }
            }
            if (includeList.Contains("orderitems"))
            {
                x = x.Include(x => x.OrderItems);
                if (includeList.Contains("orderitems.order"))
                {
                    x = x.Include(x => x.OrderItems).ThenInclude(x => x.Order);
                }
                if (includeList.Contains("orderitems.review"))
                {
                    x = x.Include(x => x.OrderItems).ThenInclude(x => x.Review);
                }
            }
            return x;
        };

        var items = await _unitOfWork.BookRepository.GetPagedResultAsync(
            filter: filter,
            include: include,
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<BookDto>>(items);
        return pagedItems;
    }

    public async Task<PagedResult<BookDto>> Search(string? search, int pageIndex = 1, int pageSize = 10)
    {
        var result = await _unitOfWork.BookRepository.GetPagedResultAsync(
            filter: search != null ? x => (x.Title != null && x.Title.Contains(search))
                || (x.Author != null && x.Author.Contains(search))
                || (x.Genre != null && x.Genre.Contains(search)) : null,
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<BookDto>>(result);
        return pagedItems;
    }

    public async Task<BookDto?> GetOne(string id)
    {
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (book == null)
            return null;
        var result = _mapper.Map<BookDto>(book);
        return result;
    }

    public async Task<BookDto> Create(BookCreateDto dto)
    {
        if (!_currentUserService.IsAdmin)
            throw new ServiceException("Only admin can add books");
        var book = _mapper.Map<Book>(dto);
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.BookRepository.AddAsync(book);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't add book, {ex.Message}", ex);
        }
        var result = _mapper.Map<BookDto>(book);
        await SendMail(result);
        return result;
    }

    public async Task<ResponseTypeId> Delete(string id)
    {
        if (!_currentUserService.IsAdmin)
            throw new ServiceException("Only admin can delete books");
        var book = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (book == null)
            throw new ServiceException("Not found the book");
        if (book.Status == "Deleted")
            throw new ServiceException("The book has been deleted");
        book.Status = "Deleted";
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.BookRepository.SoftRemove(book);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete book", ex);
        }
        var result = _mapper.Map<BookDto>(book);
        return new ResponseTypeId { id = result.Id };
    }

    public async Task<BookDto?> Update(string id, BookUpdateDto dto)
    {
        if (!_currentUserService.IsAdmin)
            throw new ServiceException("Only admin can update books");
        dto.Id = id;
        var item = await _unitOfWork.BookRepository.FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (item == null)
            return null;
        item = _mapper.Map<Book>(dto);
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.BookRepository.Update(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't update book", ex);
        }
        var result = _mapper.Map<BookDto>(item);
        return result;
    }

    // Send mail to all users
    private async Task SendMail(BookDto dto)
    {
        var users = await _unitOfWork.UserRepository.GetAllAsync(x => x.Email != null, include: x => x.Include(y => y.UserCredential));
        if (users == null)
            throw new ServiceException("No users found!!!");
        var emails = new List<string>();
        foreach (var user in users)
        {
            var email = user.Email;
            var role = user.UserCredential.Role;
            if (!string.IsNullOrEmpty(email) && role != "Admin")
                emails.Add(email);
        }
        if (emails.Count == 0)
            throw new ServiceException("No emails found!!!");
        var subject = "New Book Release";
        string content = "<h3>New Book Release:</h3>" +
              $"<p><strong>Title:</strong> {dto.Title}</p>" +
              $"<p><strong>Author:</strong> {dto.Author}</p>" +
              $"<p><strong>Genre:</strong> {dto.Genre}</p>" +
              $"<p><strong>Price:</strong> {dto.Price} Đ</p>" +
              $"<p><strong>Inventory:</strong> {dto.Quantity}</p>" +
              $"<img src=\"{dto.CoverImage}\" alt=\"Book Cover Image\" style=\"max-width: 200px;\" alt=\"{dto.Title}\">" +
              "<p><a href=\"#\">Visit our website</a> to purchase this book and browse our other titles.</p>";

        _emailService.SendMail(emails: emails, subject: subject, content: content);
    }
}
