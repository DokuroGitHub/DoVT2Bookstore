﻿using _3.Share;
using _4.BusinessLayer.Services.IServices;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;

namespace _4.BusinessLayer.Services;

public class MailService : IMailService
{
    private readonly IHttpClientFactory _clientFactory;
    private readonly Appsettings _appsettings;

    public MailService(IHttpClientFactory clientFactory, Appsettings appsettings)
    {
        _clientFactory = clientFactory;
        _appsettings = appsettings;
    }

    public async Task<string> SendMailMsGraph(string email)
    {
        const string uri = "https://graph.microsoft.com/v1.0/me/sendMail";
        var request = new HttpRequestMessage(HttpMethod.Post, uri);
        var message = new
        {
            subject = "Thông báo từ DoVT2 Bookstore",
            body = new
            {
                contentType = "Text",
                content = "Bạn có đơn hàng mowisF"
            },
            toRecipients = new List<dynamic>(){
                new
                {
                    emailAddress = new
                    {
                        address = email
                    }
                }
            }
        };
        request.Content = new StringContent(JsonConvert.SerializeObject(message), Encoding.UTF8, "application/json");
        var client = _clientFactory.CreateClient("DoVT2Bookstore");
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _appsettings.MsGraph.AccessToken);
        var response = await client.SendAsync(request);

        try
        {
            string responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine($"Response body: {responseBody}");
            return responseBody;
        }
        catch (Exception e)
        {
            return e.ToString();
        }
    }

    public void SendMail(IEnumerable<string> emails, string subject, string content)
    {
        var message = new MailMessage
        {
            From = new MailAddress(_appsettings.MailSettings.Mail),
            Subject = subject ?? "Thông báo từ DoVT2 Bookstore",
            Body = content ?? $@"
<html>
    <body>
        <h1>{subject}</h1>
        <p>{content}</p>
    </body>
</html>",
            IsBodyHtml = true
        };
        foreach (var item in emails)
        {
            message.To.Add(new MailAddress(item));
        }

        var smtpClient = new SmtpClient("smtp.gmail.com")
        {
            Port = 587,
            Credentials = new NetworkCredential(_appsettings.MailSettings.Mail, _appsettings.MailSettings.Password),
            EnableSsl = true,
        };

        smtpClient.Send(message);
    }

    public async Task SendMailAsync(IEnumerable<string> emails, string subject, string content)
    {
        var message = new MailMessage
        {
            From = new MailAddress(_appsettings.MailSettings.Mail),
            Subject = subject ?? "Thông báo từ DoVT2 Bookstore",
            Body = content ?? $@"
<html>
    <body>
        <h1>{subject}</h1>
        <p>{content}</p>
    </body>
</html>",
            IsBodyHtml = true
        };
        foreach (var item in emails)
        {
            message.To.Add(new MailAddress(item));
        }

        var smtpClient = new SmtpClient("smtp.gmail.com")
        {
            Port = 587,
            Credentials = new NetworkCredential(_appsettings.MailSettings.Mail, _appsettings.MailSettings.Password),
            EnableSsl = true,
        };

        await smtpClient.SendMailAsync(message);
    }
}
