using _1.Domain.Entities;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Services.IServices;
using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.Cart;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace _4.BusinessLayer.Services;

public class CartService : ICartService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly ICurrentUserService _currentUserService;

    public CartService(
        IUnitOfWork unitOfWork,
        IMapper mapper,
        ICurrentUserService currentUserService)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _currentUserService = currentUserService;
    }

    public async Task<PagedResult<CartDto>> GetMyCarts(int pageIndex, int pageSize)
    {
        var result = await _unitOfWork.CartRepository.GetPagedResultAsync(
            filter: x => x.UserId == _currentUserService.CurrentUserId,
            include: x => x.Include(x => x.CartItems).ThenInclude(x => x.Book),
            pageIndex: pageIndex,
            pageSize: pageSize);
        var pagedItems = _mapper.Map<PagedResult<CartDto>>(result);
        return pagedItems;
    }

    public async Task<CartDto?> GetOne(string id)
    {
        var item = await _unitOfWork.CartRepository.FirstOrDefaultAsync(
            filter: x => x.Id == id &&
                (_currentUserService.IsAdmin || x.UserId == _currentUserService.CurrentUserId));
        if (item == null)
            return null;
        var result = _mapper.Map<CartDto>(item);
        return result;
    }

    public async Task<CartDto> Create(CartCreateDto dto)
    {
        var cart = _mapper.Map<Cart>(dto);
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.CartRepository.AddAsync(cart);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't add cart, {ex.Message}", ex);
        }
        var result = _mapper.Map<CartDto>(cart);
        return result;
    }

    public async Task<ResponseTypeId> Delete(string id)
    {
        var item = await _unitOfWork.CartRepository.FirstOrDefaultAsync(x => x.Id == id);
        if (item == null)
            throw new ServiceException("Not found the cart");
        if (!_currentUserService.IsAdmin && _currentUserService.CurrentUserId != item.UserId)
            throw new ServiceException("You don't have permission to delete this cart");
        try
        {
            _unitOfWork.BeginTransaction();
            await _unitOfWork.CartItemRepository.RemoveRangeAsync(x => x.CartId == item.Id);
            _unitOfWork.CartRepository.Remove(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException("Can't delete cart", ex);
        }
        var result = _mapper.Map<CartDto>(item);
        return new ResponseTypeId { id = result.Id };
    }

    public async Task<CartDto> Update(string id, CartUpdateDto dto)
    {
        dto.Id = id;
        var item = await _unitOfWork.CartRepository.FirstOrDefaultAsync(x => x.Id == dto.Id);
        if (item == null)
            throw new ServiceException("Not found the cart");
        if (!_currentUserService.IsAdmin && _currentUserService.CurrentUserId != item.UserId)
            throw new ServiceException("You don't have permission to update this cart");
        item.Currency = dto.Currency ?? item.Currency;
        try
        {
            _unitOfWork.BeginTransaction();
            _unitOfWork.CartRepository.Update(item);
            await _unitOfWork.CommitAsync();
        }
        catch (Exception ex)
        {
            _unitOfWork.Rollback();
            throw new ServiceException($"Can't update cart, {ex.Message}", ex);
        }
        var result = _mapper.Map<CartDto>(item);
        return result;
    }

}
