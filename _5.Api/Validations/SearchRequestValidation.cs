using _3.Share.ViewModels.Book;
using FluentValidation;

namespace _5.Api.Validations;

public class SearchRequestValidation : AbstractValidator<BookSearchRequest>
{
    public SearchRequestValidation()
    {
        RuleFor(x => x.Title)
         .NotEmpty().WithMessage("Title cannot be empty.")
         .Length(1, 100).WithMessage("Title must be between 1 and 100 characters.");

        RuleFor(x => x.Description)
            .NotEmpty().WithMessage("Description cannot be empty.")
            .Length(1, 100).WithMessage("Description must be between 1 and 100 characters.");

        RuleFor(x => x.Author)
                    .NotEmpty().WithMessage("Author cannot be empty.")
                    .Length(1, 100).WithMessage("Author must be between 1 and 100 characters.");
    }
}
