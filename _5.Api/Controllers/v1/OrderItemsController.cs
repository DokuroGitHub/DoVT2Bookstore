using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.OrderItem;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers.v1;

[ApiController]
[ApiVersion("1.0", Deprecated = true)]
[Route("api/v{version:apiVersion}/[controller]")]
public class OrderItemsController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IOrderItemService _orderItemService;

    public OrderItemsController(IOrderItemService orderItemService, IMapper mapper)
    {
        _mapper = mapper;
        _orderItemService = orderItemService;
    }

    [HttpGet("my-order-items")]
    [Authorize]
    [ProducesResponseType(typeof(PagedResult<OrderItemDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetMyOrderItems(
        [FromQuery] string? fields = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _orderItemService.GetMyOrderItems(
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        if (string.IsNullOrEmpty(fields))
            return Ok(pagedItems);
        var pagedResult = new PagedResult<object>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("{orderId}/{bookId}", Name = nameof(OrderItemsController) + nameof(GetOne))]
    [Authorize]
    [ProducesResponseType(typeof(OrderItemDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetOne(
        string orderId,
        string bookId,
        [FromQuery] string? fields = null)
    {
        if (string.IsNullOrEmpty(orderId))
        {
            ModelState.AddModelError("orderId", $"orderId: {orderId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        var item = await _orderItemService.GetOne(orderId, bookId);
        if (item == null)
            return NotFound(new { orderId, bookId, message = "NotFound" });
        if (string.IsNullOrEmpty(fields))
            return Ok(item);
        var result = Utilities.CreateAnonymousObject(item, fields);
        return Ok(result);
    }

    [HttpPost]
    [Authorize]
    [ProducesResponseType(typeof(OrderItemDto), StatusCodes.Status201Created)]
    public async Task<IActionResult> Create([FromBody] OrderItemCreateDto itemDto)
    {
        var createdItem = await _orderItemService.Create(itemDto);
        return CreatedAtRoute(nameof(BooksController) + nameof(GetOne), new { id = "idk how to route" }, createdItem);
    }

    [HttpDelete("{orderId}/{bookId}")]
    [Authorize]
    [ProducesResponseType(typeof(ResponseTypeId), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(string orderId, string bookId)
    {
        if (string.IsNullOrEmpty(orderId))
        {
            ModelState.AddModelError("orderId", $"orderId: {orderId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        var deletedItem = await _orderItemService.Delete(orderId, bookId);
        if (deletedItem == null)
            return NotFound(new { orderId, bookId, message = "NotFound" });
        return Ok(deletedItem);
    }

    [HttpPut("{orderId}/{bookId}")]
    [Authorize]
    [ProducesResponseType(typeof(OrderItemDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Update(string orderId, string bookId, [FromBody] OrderItemUpdateDto itemDto)
    {
        if (string.IsNullOrEmpty(orderId))
        {
            ModelState.AddModelError("orderId", $"orderId: {orderId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        var updatedItem = await _orderItemService.Update(orderId, bookId, itemDto);
        if (updatedItem == null)
            return NotFound(new { orderId, bookId, message = "NotFound" });
        return Ok(updatedItem);
    }

    [HttpPatch("{id}")]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> UpdatePartial(string orderId, string bookId, JsonPatchDocument<OrderItemUpdateDto> patchDto)
    {
        if (string.IsNullOrEmpty(orderId))
        {
            ModelState.AddModelError("orderId", $"orderId: {orderId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (patchDto == null || !ModelState.IsValid)
        {
            ModelState.AddModelError("item", "ModelState is not valid");
            return BadRequest(ModelState);
        }
        var updateDto = new OrderItemUpdateDto();
        patchDto.ApplyTo(updateDto, ModelState);
        var updatedItem = await _orderItemService.Update(orderId, bookId, updateDto);
        if (updatedItem == null)
            return NotFound(new { orderId, bookId, message = "NotFound" });
        return NoContent();
    }
}
