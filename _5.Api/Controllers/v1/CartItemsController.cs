using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.CartItem;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers.v1;

[ApiController]
[ApiVersion("1.0", Deprecated = true)]
[Route("api/v{version:apiVersion}/[controller]")]
public class CartItemsController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly ICartItemService _cartItemService;

    public CartItemsController(ICartItemService cartItemService, IMapper mapper)
    {
        _mapper = mapper;
        _cartItemService = cartItemService;
    }

    [HttpGet("my-cart-items")]
    [Authorize]
    [ProducesResponseType(typeof(PagedResult<CartItemDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetMyCartItems(
        [FromQuery] string? fields = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _cartItemService.GetMyCartItems(
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        if (string.IsNullOrEmpty(fields))
            return Ok(pagedItems);
        var pagedResult = new PagedResult<object>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("{cartId}/{bookId}", Name = nameof(CartItemsController) + nameof(GetOne))]
    [Authorize]
    [ProducesResponseType(typeof(CartItemDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetOne(
        string cartId,
        string bookId,
        [FromQuery] string? fields = null)
    {
        if (string.IsNullOrEmpty(cartId))
        {
            ModelState.AddModelError("cartId", $"cartId: {cartId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        var item = await _cartItemService.GetOne(cartId, bookId);
        if (item == null)
            return NotFound(new { cartId, bookId, message = "NotFound" });
        if (string.IsNullOrEmpty(fields))
            return Ok(item);
        var result = Utilities.CreateAnonymousObject(item, fields);
        return Ok(result);
    }

    [HttpPost]
    [Authorize]
    [ProducesResponseType(typeof(CartItemDto), StatusCodes.Status201Created)]
    public async Task<IActionResult> Create([FromBody] CartItemCreateDto itemDto)
    {
        var createdItem = await _cartItemService.Create(itemDto);
        return CreatedAtRoute(nameof(BooksController) + nameof(GetOne), new { id = "idk how to route" }, createdItem);
    }

    [HttpDelete("{cartId}/{bookId}")]
    [Authorize]
    [ProducesResponseType(typeof(ResponseTypeId), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(string cartId, string bookId)
    {
        if (string.IsNullOrEmpty(cartId))
        {
            ModelState.AddModelError("cartId", $"cartId: {cartId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        var deletedItem = await _cartItemService.Delete(cartId, bookId);
        if (deletedItem == null)
            return NotFound(new { cartId, bookId, message = "NotFound" });
        return Ok(deletedItem);
    }

    [HttpPut("{cartId}/{bookId}")]
    [Authorize]
    [ProducesResponseType(typeof(CartItemDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Update(string cartId, string bookId, [FromBody] CartItemUpdateDto itemDto)
    {
        if (string.IsNullOrEmpty(cartId))
        {
            ModelState.AddModelError("cartId", $"cartId: {cartId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        var updatedItem = await _cartItemService.Update(cartId, bookId, itemDto);
        if (updatedItem == null)
            return NotFound(new { cartId, bookId, message = "NotFound" });
        return Ok(updatedItem);
    }

    [HttpPatch("{id}")]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> UpdatePartial(string cartId, string bookId, JsonPatchDocument<CartItemUpdateDto> patchDto)
    {
        if (string.IsNullOrEmpty(cartId))
        {
            ModelState.AddModelError("cartId", $"cartId: {cartId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(bookId))
        {
            ModelState.AddModelError("bookId", $"bookId: {bookId}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (patchDto == null || !ModelState.IsValid)
        {
            ModelState.AddModelError("item", "ModelState is not valid");
            return BadRequest(ModelState);
        }
        var updateDto = new CartItemUpdateDto();
        patchDto.ApplyTo(updateDto, ModelState);
        var updatedItem = await _cartItemService.Update(cartId, bookId, updateDto);
        if (updatedItem == null)
            return NotFound(new { cartId, bookId, message = "NotFound" });
        return NoContent();
    }
}
