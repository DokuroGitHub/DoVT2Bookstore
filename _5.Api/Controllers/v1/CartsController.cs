using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.Cart;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers.v1;

[ApiController]
[ApiVersion("1.0", Deprecated = true)]
[Route("api/v{version:apiVersion}/[controller]")]
public class CartsController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly ICartService _cartService;

    public CartsController(ICartService cartService, IMapper mapper)
    {
        _mapper = mapper;
        _cartService = cartService;
    }

    [HttpGet("my-carts")]
    [Authorize]
    [ProducesResponseType(typeof(PagedResult<CartDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetMyCarts(
        [FromQuery] string? fields = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _cartService.GetMyCarts(
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        if (string.IsNullOrEmpty(fields))
            return Ok(pagedItems);
        var pagedResult = new PagedResult<object>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("{id}", Name = nameof(CartsController) + nameof(GetOne))]
    [Authorize]
    [ProducesResponseType(typeof(CartDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetOne(
        string id,
        [FromQuery] string? fields = null)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, Is not Valid");
            return BadRequest(ModelState);
        }
        var item = await _cartService.GetOne(id);
        if (item == null)
            return NotFound(new { id, message = "NotFound" });
        if (string.IsNullOrEmpty(fields))
            return Ok(item);
        var result = Utilities.CreateAnonymousObject(item, fields);
        return Ok(result);
    }

    [HttpPost]
    [Authorize]
    [ProducesResponseType(typeof(CartDto), StatusCodes.Status201Created)]
    public async Task<IActionResult> Create([FromBody] CartCreateDto itemDto)
    {
        var createdItem = await _cartService.Create(itemDto);
        return CreatedAtRoute(nameof(BooksController) + nameof(GetOne), new { id = "idk how to route" }, createdItem);
    }

    [HttpDelete("{id}")]
    [Authorize]
    [ProducesResponseType(typeof(ResponseTypeId), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(string id)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        var deletedItem = await _cartService.Delete(id);
        if (deletedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return Ok(deletedItem);
    }

    [HttpPut("{id}")]
    [Authorize]
    [ProducesResponseType(typeof(CartDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Update(string id, [FromBody] CartUpdateDto itemDto)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        var updatedItem = await _cartService.Update(id, itemDto);
        if (updatedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return Ok(updatedItem);
    }

    [HttpPatch("{id}")]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> UpdatePartial(string id, JsonPatchDocument<CartUpdateDto> patchDto)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        if (patchDto == null || !ModelState.IsValid)
        {
            ModelState.AddModelError("item", "ModelState is not valid");
            return BadRequest(ModelState);
        }
        var updateDto = new CartUpdateDto();
        patchDto.ApplyTo(updateDto, ModelState);
        var updatedItem = await _cartService.Update(id, updateDto);
        if (updatedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return NoContent();
    }
}
