using _3.Share.Commons;
using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.Book;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers.v1;

[ApiController]
[ApiVersion("1.0", Deprecated = true)]
[Route("api/v{version:apiVersion}/[controller]")]
public class BooksController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IBookService _bookService;

    public BooksController(IBookService bookService, IMapper mapper)
    {
        _mapper = mapper;
        _bookService = bookService;
    }

    [HttpGet]
    [ResponseCache(CacheProfileName = "Default30")]
    [ProducesResponseType(typeof(PagedResult<BookDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetPagedItems(
        [FromQuery] string? search = null,
        [FromQuery] string? includes = null,
        [FromQuery] string? fields = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _bookService.GetPagedItems(
            search: search,
            includeProperties: includes,
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        if (string.IsNullOrEmpty(fields))
            return Ok(pagedItems);
        var pagedResult = new PagedResult<object>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("advanced-search")]
    [ResponseCache(CacheProfileName = "Default30")]
    [ProducesResponseType(typeof(PagedResult<BookDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> AdvancedSearch(
        [FromQuery] string? t = null,
        [FromQuery] string? d = null,
        [FromQuery] string? a = null,
        [FromQuery] string? g = null,
        [FromQuery] string? s = null,
        [FromQuery] string? includes = null,
        [FromQuery] string? fields = null,
        [FromQuery] int pageSize = 10,
        [FromQuery] int pageIndex = 1)
    {
        if (pageSize < 1)
        {
            ModelState.AddModelError("pageSize", $"pageSize: {pageSize}, Is not Valid");
            return BadRequest(ModelState);
        }
        if (pageIndex < 1)
        {
            ModelState.AddModelError("pageNumber", $"pageNumber: {pageIndex}, Is not Valid");
            return BadRequest(ModelState);
        }
        var pagedItems = await _bookService.AdvancedSearch(
            title: t,
            description: d,
            author: a,
            genre: g,
            status: s,
            includeProperties: includes,
            pageIndex: pageIndex,
            pageSize: pageSize);
        Response.Headers.Add("X-Pagination", pagedItems.ToString());
        if (string.IsNullOrEmpty(fields))
            return Ok(pagedItems);
        var pagedResult = new PagedResult<object>(
            items: pagedItems.Items.Select(x => Utilities.CreateAnonymousObject(x, fields)).ToList(),
            count: pagedItems.TotalCount,
            pageIndex,
            pageSize);
        return Ok(pagedResult);
    }

    [HttpGet("{id}", Name = nameof(BooksController) + nameof(GetOne))]
    [ResponseCache(Duration = 30, Location = ResponseCacheLocation.None, NoStore = true)]
    [ProducesResponseType(typeof(BookDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetOne(
        string id,
        [FromQuery] string? fields = null)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, Is not Valid");
            return BadRequest(ModelState);
        }
        var item = await _bookService.GetOne(id);
        if (item == null)
            return NotFound(new { id, message = "NotFound" });
        if (string.IsNullOrEmpty(fields))
            return Ok(item);
        var result = Utilities.CreateAnonymousObject(item, fields);
        return Ok(result);
    }

    [HttpPost]
    // [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(BookDto), StatusCodes.Status201Created)]
    public async Task<IActionResult> Create([FromBody] BookCreateDto itemDto)
    {
        var createdItem = await _bookService.Create(itemDto);
        return CreatedAtRoute(nameof(BooksController) + nameof(GetOne), new { id = "idk how to route" }, createdItem);
    }

    [HttpDelete("{id}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(ResponseTypeId), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(string id)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        var deletedItem = await _bookService.Delete(id);
        if (deletedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return Ok(deletedItem);
    }

    [HttpPut("{id}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(BookDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Update(string id, [FromBody] BookUpdateDto itemDto)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        var updatedItem = await _bookService.Update(id, itemDto);
        if (updatedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return Ok(updatedItem);
    }

    [HttpPatch("{id}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<IActionResult> UpdatePartial(string id, JsonPatchDocument<BookUpdateDto> patchDto)
    {
        if (string.IsNullOrEmpty(id))
        {
            ModelState.AddModelError("id", $"id: {id}, is not valid");
            return BadRequest(ModelState);
        }
        if (patchDto == null || !ModelState.IsValid)
        {
            ModelState.AddModelError("item", "ModelState is not valid");
            return BadRequest(ModelState);
        }
        var updateDto = new BookUpdateDto();
        patchDto.ApplyTo(updateDto, ModelState);
        var updatedItem = await _bookService.Update(id, updateDto);
        if (updatedItem == null)
            return NotFound(new { id, message = "NotFound" });
        return NoContent();
    }
}
