﻿using _3.Share.IServices;
using _3.Share.ViewModels;
using _3.Share.ViewModels.User;
using Microsoft.AspNetCore.Mvc;

namespace _5.Api.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/[controller]")]
[ApiVersionNeutral]
public class AuthController : ControllerBase
{
    private readonly IAuthService _authService;

    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }

    [HttpPost("login")]
    [ProducesResponseType(typeof(LoginResponseDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Login([FromBody] LoginRequestDto model)
    {
        if (!ModelState.IsValid || model == null)
        {
            ModelState.AddModelError("model", "Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(model.Username))
        {
            ModelState.AddModelError("UserName", "Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(model.Password))
        {
            ModelState.AddModelError("Password", "Is not Valid");
            return BadRequest(ModelState);
        }

        var loginResponse = await _authService.LoginAsync(model);
        if (loginResponse == null || string.IsNullOrEmpty(loginResponse.Token))
        {
            ModelState.AddModelError("LoginRequest", "Failed to login");
            return BadRequest(ModelState);
        }
        return Ok(loginResponse);
    }

    [HttpPost("register")]
    [ProducesResponseType(typeof(UserFlatDto), StatusCodes.Status200OK)]
    public async Task<IActionResult> Register([FromBody] RegisterationRequestDto dto)
    {
        if (!ModelState.IsValid || dto == null)
        {
            ModelState.AddModelError("model", "Is not Valid");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(dto.Email))
        {
            ModelState.AddModelError("Email", "Is not Valid");
            return BadRequest(ModelState);
        }
        bool ifUserNameUnique = await _authService.IsUniqueUserAsync(dto.Email);
        if (!ifUserNameUnique)
        {
            ModelState.AddModelError("Email", "Email already exists");
            return BadRequest(ModelState);
        }
        if (string.IsNullOrEmpty(dto.Password))
        {
            ModelState.AddModelError("Password", "Is not Valid");
            return BadRequest(ModelState);
        }
        if (dto.Password != dto.ConfirmPassword)
        {
            ModelState.AddModelError("ConfirmPassword", "Is not Valid");
            return BadRequest(ModelState);
        }
        if (dto.Role == "Admin")
        {
            ModelState.AddModelError("Role", "Admins can only be created by the system");
            return BadRequest(ModelState);
        }
        var user = await _authService.RegisterAsync(dto);
        if (user == null || string.IsNullOrEmpty(user.Id))
        {
            ModelState.AddModelError("RegisterationRequest", "Failed to register");
            return BadRequest(ModelState);
        }
        return Ok(user);
    }
}
