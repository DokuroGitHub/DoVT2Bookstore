﻿using _2.Domain2Db;
using _3.DataAccessLayer;
using _3.DataAccessLayer.Repositories;
using _3.DataAccessLayer.Repositories.IRepositories;
using _3.DataAccessLayer.Services;
using _3.DataAccessLayer.Services.IServices;
using Microsoft.EntityFrameworkCore;

namespace _5.Api.ServicesExtensions;

public static class DataAccessLayerServices
{
    public static IServiceCollection AddDataAccessLayerServices(this IServiceCollection services, _3.Share.Appsettings appsettings)
    {
        // mapper
        services.AddAutoMapper(typeof(MappingConfig).Assembly);

        // add services
        services.AddScoped<ICurrentTimeService, CurrentTimeService>();
        services.AddScoped<ICurrentUserService, CurrentUserService>();
        services.AddScoped<IJwtService, JwtService>();

        // Repositories
        services.AddScoped<IBookRepository, BookRepository>();
        services.AddScoped<ICartRepository, CartRepository>();
        services.AddScoped<ICartItemRepository, CartItemRepository>();
        services.AddScoped<IOrderRepository, OrderRepository>();
        services.AddScoped<IOrderItemRepository, OrderItemRepository>();
        services.AddScoped<IUserCredentialRepository, UserCredentialRepository>();
        services.AddScoped<IUserRepository, UserRepository>();

        // UnitOfWork
        services.AddScoped<IUnitOfWork, UnitOfWork>();

        // DbContext
        services.AddDbContext<ApplicationDbContext>(option => option.UseSqlServer(appsettings.ConnectionStrings.DokuroSQLConnectionString));
        // services.AddDbContext<ApplicationDbContext>(option => option.UseInMemoryDatabase("DokuroDb"));
        services.AddMemoryCache();

        return services;
    }
}
