﻿using _3.Share.IServices;
using _4.BusinessLayer.Services;
using _4.BusinessLayer.Services.IServices;

namespace _5.Api.ServicesExtensions;

public static class InfrastructureServices
{
    public static IServiceCollection AddBusinessLayerServices(this IServiceCollection services, _3.Share.Appsettings appsettings)
    {
        // add services
        services.AddScoped<IAuthService, AuthService>();
        services.AddHttpClient<IMailService, MailService>();
        services.AddScoped<IMailService, MailService>();
        services.AddScoped<IBookService, BookService>();
        services.AddScoped<ICartService, CartService>();
        services.AddScoped<ICartItemService, CartItemService>();
        services.AddScoped<IOrderService, OrderService>();
        services.AddScoped<IOrderItemService, OrderItemService>();

        return services;
    }
}
