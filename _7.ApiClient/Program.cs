using ApiClient;
using ApiClient.ServicesExtensions;

var builder = WebApplication.CreateBuilder(args);

//* Add services to the container.
// appsettings.json
var _appsettings = builder.Configuration.Get<Appsettings>() ?? new Appsettings();
// services
builder.Services.AddWebApiServices(_appsettings);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
