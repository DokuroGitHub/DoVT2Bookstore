using Microsoft.AspNetCore.Mvc;
using Application.ViewModels.Book;
using Application.IServices;

namespace ApiClient.Controllers;

[ApiController]
[Route("[controller]")]
public class BooksController : ControllerBase
{
    private IBookService _bookService;

    public BooksController(IBookService bookService)
    {
        _bookService = bookService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(List<BookDto>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetBooks()
    {
        var items = await _bookService.GetAll(x => new { x.Id, x.Title, x.Description });
        return Ok(items);
    }
}
