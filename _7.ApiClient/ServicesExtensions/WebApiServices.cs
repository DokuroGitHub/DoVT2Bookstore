﻿using ApiClient.Middlewares;

namespace ApiClient.ServicesExtensions;

public static class WebApiServices
{
    public static IServiceCollection AddWebApiServices(this IServiceCollection services, Appsettings appsettings)
    {
        // add cors
        services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy", builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        });

        // add controllers
        services.AddControllers().AddNewtonsoftJson();//.AddXmlSerializerFormatters();

        // add middlewares
        services.AddSingleton(appsettings);
        services.AddSingleton<ExceptionMiddleware>();

        // add authorization
        services.AddAuthorization();

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();

        return services;
    }
}
