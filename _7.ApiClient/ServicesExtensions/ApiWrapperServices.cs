﻿using ApiWrapper;
using Application.IServices;

namespace ApiClient.ServicesExtensions;

public static class ApiWrapperServices
{
    public static IServiceCollection AddInApiWrapperServices(this IServiceCollection services, Appsettings appsettings)
    {
        // add services
        services.AddScoped<IBookService, BookService>();

        return services;
    }
}
